Coding Hints:

** Creating a new object

Objects are created in the third section of code.  (See the tutorial for details on the sections).  Object
creation follows this form:

$objectname =& new Object();
$objectname->oid = "objectname";
$_allobjs['objectname'] =& $objectname;

All the names must match, otherwise there will be problems.

In addition, you should supply at least a name and a description using the name
and desc property, e.g.
$objectname->name = "my object";
$objectname->desc = "An amazing object!";

Object names starting with _ are reserved.

** Passing objects as function parameters, or assigning them

By default, PHP copies objects.  This will cause a problem if you want to make changes.  So,
for any function that will make a change or will call a function that will make a change, the
objects should be passed in by reference.

function myfunc(&$object) { ... }

Also, if you are doing object assignments, do them by reference:

$newobj =& $oldobj;

Keep in mind then that any changes made to newobj will be reflected in oldobj.

** Dynamic content.

There are two ways to do dynamic content.  All dynamic content can be done
in derived classes -- this is the most straightforward approach, but it requires
having an extra class for each type of dynamic content.

For very simple kinds of dynamic content (like showing open or closed in a room
description), you can use dynamic inline code.  This is done by putting part of the
description into '<< ... >>'  The << and >> trigger the detection of inline code and
the single quotes mean that variables will not be evaluated immediately.  The code
will be run in a function seperated from the class (so you cannot use $this).  
You should return the string you want to output.  Example:

$room->desc = "You are in a room with a door that is " . '<< global $_allobjs;
return ($_allobjs['mydoor']->isopen ? "open" : "closed");>>' . ".";

Do not attempt to use dynamic content functions (such as objRef, withMe, etc)
in the third section if not in << ... >> dynamic inline code,
since all this is executed and finished at the very beginning
 of the game.  When in doubt, put it in a derived class.

** Capitalization

Room names (i.e. the name property) are generally capitalized.
Object names are generally lowercase (unless they are proper names,
like for an NPC).  Descriptions always begin with a capital.

** Classes
	The following are all currently defined classes and where the commentary can be found:
	
	Name				File					Line #
	Object				ad_base.php			188
	Fuse					ad_base.php			697
	Game				ad_base.php			727
	Room					ad_classes.php			22
	Darkroom				ad_classes.php			258
	Carryable				ad_classes.php			361
	Container				ad_classes.php			535
	Surface				ad_classes.php			665
	Openable				ad_classes.php			734
	Switchable				ad_classes.php			832
	NoEntranceRoom		ad_classes.php			923
	Lockable				ad_classes.php			984
	Door					ad_classes.php			1,126
	MessageFuse			ad_classes.php			1,254
	NPC					ad_npc.php			22
	



