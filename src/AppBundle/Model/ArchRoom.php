<?php

/***
 PHPAdventure!   Copyright (C) 2003 Steven Kollmansberger

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 ***/

namespace AppBundle\Model;

use phpadventure\Room;

class ArchRoom extends Room {
	// for the arch, we want to repel people if they aren't carrying the skull
	// otherwise, they win the game
	function enterRoom(&$origoid) {
		// we want to "bounce" the user back if they aren't carrying the skull
		// otherwise, win the game

		global $_allobjs; // this statement is needed to be able to access objects other than the current one.
		// In particular, we need access to special object _Game.

		if ($_allobjs['goldskull']->isIn($_allobjs['_Me'])) { // we can use the location property to find out where
			// something is.  You should NEVER set this
			// property, always use moveInto instead.
			// isIn includes containers too
			// Don't use withMe because
			// that includes the room as well

			// in this case, win the game
			// Display a message
			print "<div class='descpane'>The archway accepts the golden skull in exchange for your passage to freedom.  Where will you go next?  What adventures will you have?  Only <b>you</b> can decide!</div>";

			// The _Game object has a member function called win that ends the game.
			// This format: $_allobjs['objectname'] can be used to access the properties
			// or member functions of any object.
			$_allobjs['_Game']->win();

		} else {
			// they don't have it!

			$origoid = NULL; // this disables transit

			// returned text is auto-formatted
			return "As your approach the glowing archway, a high-pitched squeal shocks your ears.  A voice booms, \"Only the holder of the golden skull may pass!\"";


		}


	}

}
