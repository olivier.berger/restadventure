<?php

/***
 PHPAdventure!   Copyright (C) 2003 Steven Kollmansberger

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 ***/

namespace AppBundle\Model;

use phpadventure\Surface;

// Declare your custom classes for this game here!
class TrappedPedestal extends Surface {
	// we want the skull to cause a trap to go off when the user picks up something
	// off the pedestal and there is nothing else there

	function contentsChanged($entered, &$obj) { // contents changed is called whenever anything is put on or taken off the pedestal

		global $_allobjs; // this statement is needed to be able to access objects other than the current one.
		// In particular, we need access to special object _Game.


		// What we need to ask in this case is are there specific items thaty work on the pedestal, or
		// is it just the presence of any item?

		// We'll go with the presence of any item - more than one item is ok too (otherwise the puzzle would be unsolvable!)

		// $this refers to the current object.  contains is the array which lists what is inside the object.  We're only concerned with
		// how many things are here, not what they are in particular
		if (count($this->contains) == 0) {
			// the trap goes off!
			// note that lifting the skull may set it off, but actually ANY object could be the last straw.
			// because of this, we don't want to refer to an object by name in the text, but instead, call the appropriate method
			// so that the message is dynamic.
			print "<div class='descpane'>As you lift " . $obj->theName() . ", a volley of poisonous arrows is shot from the walls! You try to dodge the arrows, but they take you by surprise!</div>";
			// when overloading a verb or doing any direct output, it is necessary to handle all of the formatting
			// yourself.  In this case, we are creating a description pane to show the results of the action.

			// The _Game object has a member function called death that ends the game.
			// This format: $_allobjs['objectname'] can be used to access the properties
			// or member functions of any object.
			$_allobjs['_Game']->death();


		} else parent::contentsChanged($entered, $obj); // this is VERY important!
		// In almost all your overloaded classes, you will want the default
		// behavior to take over in some cases.  When you want the
		// default behavior, use this format:
		// parent::funcname(params ... )

	}

}

