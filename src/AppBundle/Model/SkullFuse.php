<?php

/***
 PHPAdventure!   Copyright (C) 2003 Steven Kollmansberger

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 ***/

namespace AppBundle\Model;

use phpadventure\Fuse;

class SkullFuse extends Fuse {
	// a fuse is a way to cause events to occur based on turn count

	var $haswarned; // we'll set this to true once the warning has been shown

	function __construct() { // overloaded constructor called when class is first made
		$this->haswarned = FALSE;
		parent::__construct(); // whenever a constructor is overloaded, you MUST call the parent constructor!
	}

	function fuseProcess($verb, $itemoid, $dobjoid) {
		global $_allobjs;
		// In this case, the fuse will cause the skull to explode and kill the user in a couple of turns

		// note that if the skull is not in the same room as the player, the explosion doesn't affect
		// them and they don't know about it.

		if ($this->haswarned == FALSE) {
			$this->haswarned = TRUE;
			if ($_allobjs['goldskull']->withMe()) // this tells us if the skull is with the player or not!
				$r = "The skull begins to sputter and spark!  It could explode at any moment!";
				$this->fuseleft = 5; // give them two more turns
		} else {
			// curtains
			if ($_allobjs['goldskull']->withMe()) {// this tells us if the skull is with the player or not!

				SendHeader(); // because fuse processing is pre-initialization, this must be called!
				print "<div class='descpane'>The skull explodes in a violent fury!</div>";
				$_allobjs['_Game']->death();
			} else {
				$r = "You hear a muffled explosion in the distance.";
				$_allobjs['goldskull']->moveOut(); // this makes it go away
				// note that at this point the game is unwinnable.  You may wish to simply end it
				// because of this.
			}
		}

		return $r;
	}

}
