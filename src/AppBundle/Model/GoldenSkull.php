<?php

/***
 PHPAdventure!   Copyright (C) 2003 Steven Kollmansberger

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 ***/

namespace AppBundle\Model;

use phpadventure\Carryable;

class GoldenSkull extends Carryable {
	// we'll use a custom take method to give a scary message and set the fuse
	// only if they haven't seen it before.

	function doVerbHandle($verb) {
		global $_allobjs;
		if ($verb == "take") { // if they take it
			// post the warning
			print "<div class='descpane'>As you pick up the skull, you notice that it immediately becomes hot to the touch.  You think you hear something burning inside!</div>";

			// engage the fuse!
			$_allobjs['skullfuse']->fuseleft = 5; // two turns until the warning
			return parent::doVerbHandle($verb); // allow normal behavior (i.e. they can still pick it up)

		} else return parent::doVerbHandle($verb); // anything else, default behavior

	}

}
