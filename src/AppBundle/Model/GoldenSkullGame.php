<?php

/***
 PHPAdventure!   Copyright (C) 2003 Steven Kollmansberger
 				 Copyright 2016 Olivier Berger - Telecom SudParis

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 ***/

/**** The Golden Skull, a PHPAdventure! sample game based on the TADS game
 of the same name. ****/

namespace AppBundle\Model;

use Symfony\Component\DependencyInjection\ContainerInterface;

include_once '../src/phpadventure/ad_base.php';
//include_once '../src/phpadventure/ad_classes.php';
//include_once '../src/phpadventure/ad_npc.php';

// Declare your custom classes for this game here!

use phpadventure\Game;
use phpadventure\Object;
use phpadventure\Room;
use phpadventure\NPC;
use phpadventure\Carryable;

global $_allobjs;
$_allobjs = array();

function SendHeader() {
	global $_allobjs;

	// 	// First, disable cache
	// 	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
	// 	header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
	// 	header("Cache-Control: no-store, no-cache, must-revalidate");
	// 	header("Cache-Control: post-check=0, pre-check=0", false);
	// 	header("Pragma: no-cache");
	// 	// end disable cache

	$game = $_allobjs['_Game'];
	// 	print	"<html><head>";
	// 	if (!is_null($game->cssfile))
		// 		print "<link type='text/css' rel='stylesheet' href='" . $game->cssfile . "'>";
		// 	else { // default style sheet
		// 		print "<style type='text/css'>";
		// 		print "body { background: silver; margin-left: 10%; margin-right: 10%; margin-top: 10%; margin-bottom: 10%;}";
		// 		print "div.statusbar { margin-top: 2em; margin-bottom: 2em; background: gray; color: white; border: solid;  border-width: thin; width: 100%; font-weight: bold;}";
		// 		print "div.descpane { padding: 0.5em; margin-top: 2em; margin-bottom: 2em; background: #666666; color: white; border: solid;  border-width: thin; width: 100%; }";
		// 		print "div.verbpane { margin-top: 2em; margin-bottom: 2em; background: gray; color: white; border: solid;  border-width: thin; width: 100%; }";
		// 		print "div.sidebar { padding: 0.5em; margin-top: 2em; margin-bottom: 2em; background: gray; color: white; border: solid;  border-width: thin; width: 100%; }";
		// 		print "div.sideinv { padding: 0.5em; margin-top: 2em; margin-bottom: 2em; background: gray; color: white; border: solid;  border-width: thin; width: 100%; }";
		// 		print "div.continue { padding: 0.5em; margin-top: 2em; margin-bottom: 2em; background: gray; color: white; border: solid;  border-width: thin; }";
		// 		print "div.copyright { margin-top: 2em; margin-bottom: 2em; background: silver; color: black; width: 100%; }";
		// 		print ":link { color: rgb(0, 240, 240) }";
		//   		print ":visited { color: rgb(0, 240, 240) }";
		// 		print "h3 { font-weight: bold; }";
		// 		print "h4 { font-style: italic; font-size: 75%;}";
		// 		print "</style>";
		// 	}
		// 	if (!is_null($game->title)) {
		// 		print "<title>" . dynamicString($game->title) . "</title>";
		// 	} else {
		// 		print "<title>" . dynamicString($game->name) . "</title>";
		// 	}
		// 	print "</head><body>";


	$title = $_allobjs['_Me']->location;

	$title = $_allobjs[$title]->statusBar();
	$xt = "";
	$game = $_allobjs['_Game'];
	if (!is_null($_allobjs['_item'])) {
		$titem = $_allobjs['_item'];
		$tdobj = $_allobjs['_dobj'];
		if (is_null($tdobj)) {
			$xt = " (" . $titem->verbing($_allobjs['_verb']) . ")";
		} else {
			$xt = " (" . $titem->verbIoIng($_allobjs['_verb'], $tdobj) . ")";
		}
	}
	print "<div class='statusbar'><table width=100%><tr><td align=left width=33%><h3>" . $title . $xt . "</h3></td><td align=center width=33%><h3>";
	print dynamicString($game->name) . " (" . $game->version . ") </h3></td><td align=right width=33%><h3>";
	print $game->turns . " Turn" . ($game->turns == 1 ? "" : "s");
	if ($game->maxscore > 0) print "; " . $game->score . " of " . $game->maxscore . " Point" . ($game->maxscore == 1 ? "" : "s");
	print "</h3></td></tr></table></div>";
	print "<table width=100%><tr valign=top><td align=left width=75%>";
}


// function FinishPageFooter() {
// 	global $_allobjs;
// 	print "</td><td align=right><table><tr valign=top><td><div class='sidebar'>";
// 	$f = $_allobjs['_Game'];
// 	$f = $f->sysverbs;
// 	asort($f);
// 	foreach ($f as $i) {

// 		print "<a href='" . BaseURL() . "_sysverb=" . $i . "'>" . $i . "</a><br>";

// 	}
// 	//print "<a href='" . BaseURL() . "sysverb=quit'>Quit</a>";
// 	print "</div></td></tr><tr valign=bottom><td><div class='sideinv'>";
// 	print "<h3>Inventory</h3>";

// 	$me = $_allobjs['_Me'];
// 	$mc = $me->contains;
// 	asort($mc);
// 	$icnt = 0;
// 	foreach ($mc as $i) {

// 		$f =& $_allobjs[$i];
// 		if ($f->invListed) {
// 			$icnt++;
// 			print dynamicString($f->objRef("look", $f->aName()) . "<br>");
// 		}
// 	}
// 	if ($icnt == 0)
// 		print "<h4>You are carrying nothing.</h4>";

// 		print "</div></td></tr></table></table>";
// }

// function SendFooter() {
// 	global $_allobjs;
// 	print "<div class='copyright' align=center><div class='copyright'>";
// 	print "<h4>" . dynamicString($_allobjs['_Game']->footermsg) . "</h4>";
// 	print "<h4><a href='http://phpadventure.sf.net'>PHPAdventure!</a> copyright (c) 2003 <a href='mailto:steven777400@users.sourceforge.net'>Steven Kollmansberger</a></h4></div>";

// 	print "</body></html>";

// }

class GoldenSkullGame 
{
	private $container;
	
	public function __construct(ContainerInterface $container)
	{
		global $_allobjs;
	
		$this->container = $container;
		
		$sessionvars = $this->container->get('session')->get('vars');
		
		if(! $sessionvars) 
		{
			

			$_Game = new Game(); // create a default game object
			$_Game->oid = "_Game";
			$_allobjs['_Game'] = $_Game;
			
			$_Me = new Object(); // create a default Me object
			$_Me->oid = "_Me";
			$_allobjs['_Me'] =& $_Me;
			
			$_Startroom = new Room(); // create a default starting room
			$_Startroom->oid = "_Startroom";
			$_allobjs['_Startroom'] =& $_Startroom;
			
			$_Me->moveInto($_Startroom);
				
			
			
		$_Game = $_allobjs['_Game'];
		//dump($_Game);
		
		// Default game, startroom and me objects are already declared.
		
		$_Game->name = "The Golden Skull";
		$_Game->version = "1.1";
		$_Game->desc = "A sample PHPAdventure! game based on the TADS game of the same name.";
		$_Game->footermsg = "The Golden Skull translated from TADS by Steven Kollmansberger";
		$_Game->initmsg = "This game is a short sample to demonstrate how to program in PHPAdventure!";
	
		$_Startroom = $_allobjs['_Startroom'];
		
		$_Startroom->name = "Outside Cave";
		// Notice the use of the addGo member function:
		// This function allows you to create a link from one room to another.  It generates the link and also updates
		// the room's "cango" list.  The first parameter is the name of the destination room, the second is the text
		// to appear inside the link.  We use the name of the destination instead of the destination itself so
		// you can link to rooms not yet defined.
		$_Startroom->desc = "You're standing in the bright sunlight just outside of  " .
		$_Startroom->addGo("cave", "a large, dark, forboding cave").
		". Nearby, a " . $_Startroom->addGo("arch", "glowing archway") .
		" stands between you and the rest of the universe.";

		// When creating a new object of any kind, you must use the following format:
		/**
		$objectname = new Object();
		$objectname->oid = "objectname";
		$_allobjs['objectname'] =& $objectname;
		**/ // replace all instances of objectname with the name of the object
		$cave = new Room(); // Room is the type where the player can go there
		$cave->oid = "cave"; // notice the name cave matchs the name given in the addGo statement for _Startroom
		$_allobjs['cave'] =& $cave;

		// Ok, now we can assign properties to this room
		$cave->name = "Cave";
		$cave->desc = "You're inside a dark and musty cave.  Sunlight pours in from " . $cave->addGo("_Startroom", "the opening") . ". ";

		// The pedestal is a surface (you can set things on it) and it's also immoble.
		// By defaults, surfaces and other containers are carriable, so we'll need
		// to turn that off.  otherwise someone could just walk away with the pedestal!
		$pedestal = new TrappedPedestal(); // custom class to perform special actions
		$pedestal->oid = "pedestal";
		$_allobjs['pedestal'] =& $pedestal; // the usual definitions

		// First, to turn off the carryable flag.  isFixed is the flag that can disable carry options
		$pedestal->isFixed = TRUE;
		// When we set the isFixed flag to turn,  the pedestal will no longer be listed after the room
		// description.  You can use fixedObjRef to list these items in the room description, however,
		// the object must be declared before the room is created.  In this case, we will simply let
		// the system list the object for us.
		$pedestal->isListed = TRUE;

		// Now we can give it a name and description
		$pedestal->name = "pedestal";
		$pedestal->desc = "The stone pedestal is mounted in the center of the cave. ";

		// To assign an object to a room or container, use the moveInto member function
		$pedestal->moveInto($cave);


		// We want to put something on the pedestal - the prize for this game
		// A simple object that the player can carry around is called a Carryable
		// $goldskull = new Carryable();
		$goldskull = new GoldenSkull();
		$goldskull->oid = "goldskull";
		$_allobjs['goldskull'] =& $goldskull;

		$goldskull->name = "golden skull";
		$goldskull->desc = "A shiny skull that appears to be made of solid gold! ";

		// We'll put the skull on the pedestal.  The system will automatically
		// list it since we did not set the isFixed flag.
		$goldskull->moveInto($pedestal);


		// We need an object to counterbalance the skull
		$smallrock = new Carryable();
		$smallrock->oid = "smallrock";
		$_allobjs['smallrock'] =& $smallrock;

		$smallrock->name = "small rock";
		$smallrock->desc = "A small, yet relatively heavy rock. ";
		$smallrock->moveInto($cave);

		// We need a way to win the game
		// How about an arch you can walk thourhg
		$arch = new ArchRoom(); // even though you'll never be in this room, we make it a room for movement purposes
		$arch->oid = "arch";
		$_allobjs['arch'] =& $arch;
		$arch->name = "glowing archway";


		// create the fuse.  This is what will be activated when the user picks up the skull
		$skullfuse = new SkullFuse();
		$skullfuse->oid = "skullfuse";
		$_allobjs['skullfuse'] =& $skullfuse;
		$skullfuse->fuseleft = 0; // disabled by default

		// create an npc to talk about the skull
		$janedoe = new NPC();
		$janedoe->oid = "janedoe";
		$_allobjs['janedoe'] =& $janedoe;
		$janedoe->name = "Jane Doe";
		$janedoe->desc = "A perfectly ordinary looking woman sits here looking perfectly disturbed.";
		$janedoe->dontKnow = "\"Do I look like an encyclopedia?\" she asks rhetorically."; // says this when asked about something unknown
		$janedoe->randomMessages[0] = "Jane sighs and checks her watch."; // things that will happen
		$janedoe->randomMessages[1] = "Jane scowls at you."; // use blank string "" to specify nothing
		$janedoe->randomMessages[2] = "Jane twiddles her fingers.";
		$janedoe->itSelf = "herself";
		// add things that she knows a special response to in ask-about
		$janedoe->askAddObject($goldskull, "\"Ah yes,\" she begins, \"I'm supposed to tell you how exciting and great the golden skull is.  It's a skull, ok?  A chunk of bone covered in gold.  Sheesh!  Some people!\"");
		$janedoe->askAddObject($janedoe, "\"They said there was a job open for an NPC, so I'm like: 'I don't want to be a bit part like last time.'  'Oh no,' they said, 'You'll be the main NPC in the game.  Center stage.'  So here I am.  Center stage, all right.  No action roles, no intrigue, just two bit lines.  Worthless.\"  She exhales sharply and slumps down."); // '
		// you could use a conversation tree if you wanted the user to be able to pursue the conversation further.
		$janedoe->moveInto($_Startroom);
		}
		else {
			//$_f = unserialize($_SESSION['vars']);
			foreach ($sessionvars as $k => $i) {
				if (isset($$k)) continue;
			
				$$k = $i;
				if (is_a($i, "phpadventure\Object")) {
			
					$_allobjs[$k] =& $$k;
				}
			
			}
		}
	}
	
	public function prepareHeader(& $result)
	{
		global $_allobjs;
		
		$result['game'] = array();
		
			//SendHeader();
		$title = $_allobjs['_Me']->location;
		
		$title = $_allobjs[$title]->statusBar();
		$xt = "";
		$game = $_allobjs['_Game'];
		if (!is_null($_allobjs['_item'])) {
			$titem = $_allobjs['_item'];
			$tdobj = $_allobjs['_dobj'];
			if (is_null($tdobj)) {
				$xt = " (" . $titem->verbing($_allobjs['_verb']) . ")";
			} else {
				$xt = " (" . $titem->verbIoIng($_allobjs['_verb'], $tdobj) . ")";
			}
		}
		$result['game']['title'] = $title . $xt;
		$result['game']['name'] = dynamicString($game->name);
		$result['game']['version'] = $game->version;
		$result['game']['turns'] = $game->turns . " Turn" . ($game->turns == 1 ? "" : "s");
		if ($game->maxscore > 0) {
			$result['game']['score'] = "; " . $game->score . " of " . $game->maxscore . " Point" . ($game->maxscore == 1 ? "" : "s");
		}
		else { 
			$result['game']['score'] = '';
		}
	}

	public function prepareFooter(& $result)
	{
		global $_allobjs;
		
		//FinishPageFooter();
		$f = $_allobjs['_Game'];
		$f = $f->sysverbs;
		asort($f);
		
		$result['game']['footermsg'] = $_allobjs['_Game']->footermsg;
		$result['game']['sysverbs'] = $f;
		
		$inventory = array();
		
		$me = $_allobjs['_Me'];
		$mc = $me->contains;
		asort($mc);
		$icnt = 0;
		foreach ($mc as $i) {
		
			$f =& $_allobjs[$i];
			if ($f->invListed) {
				$icnt++;
				$inventory[] = dynamicString($f->htmlLinkObjRef($f->objRef("look", $f->aName())));
			}
		}
		
		//SendFooter();
		
		$result['game']['inventory'] = $inventory;
		
	}
	
	public function play($_sysverb, $_verb, $_item, $_dobj = null, $_misc = null)
	{
		$result = array('description' => '', '_interlude' => '', 'verbs' => '', 'message' => '', 'continue' => '');
		
		global $_allobjs;
		$_Game = $_allobjs['_Game'];
		
		// if null passed
		//if (!isset($_sysverb)) $_sysverb = "init";
		
		// if empty string passed
		if(! $_sysverb) $_sysverb = null;
		
		//$_sysverb = $_REQUEST['_sysverb'];
		
		
// 		$_verb = $_REQUEST['_verb'];
// 		$_item = $_REQUEST['_item'];
// 		$_dobj = $_REQUEST['_dobj'];
// 		$_misc = $_REQUEST['_misc'];
		
		if ($_sysverb == "Wait") {
			unset($_sysverb);
			$_verb = "Wait";
		}
		
		if (is_null($_sysverb)) {
			$_allobjs['_interlude'] = $_Game->turnStart($_verb, $_item, $_dobj);
		}
		
		$_allobjs['_misc'] = $_misc;
		$_allobjs['_sysverb'] = $_sysverb;
		$_allobjs['_verb'] = $_verb;
		//$_allobjs['_dobj'] = $$_dobj;
		if( ($_item) && isset($_allobjs[$_item])) {
			$_allobjs['_item'] = $_allobjs[$_item];
		}
		else {
			$_allobjs['_item'] = null;
		}
		if( ($_dobj) && isset($_allobjs[$_dobj])) {
			$_allobjs['_dobj'] = $_allobjs[$_dobj];
		}
		else {
			$_allobjs['_dobj'] = null;
		}
// 		if( ($_misc) && isset($_allobjs[$_misc])) {
// 			$_allobjs['_misc'] = $_allobjs[$_misc];
// 		}
// 		else {
// 			$_allobjs['_misc'] = null;
// 		}
				
		$this->prepareHeader($result);
		
		$_Me = $_allobjs['_Me'];
		
		$_f = $_Me->location;
		
		// Apply default action on item if a default verb is set (go, look)
		if(!$_verb) {
			if (isset($_allobjs[$_item]) && ($_item != $_f)) {
				$defaultverb = $_allobjs[$_item]->defaultVerb();
				if($defaultverb) $_verb = $defaultverb;
			}
		}
		
		if (!is_null($_sysverb)) {
		
		
		
			$this->handleSysVerb($_sysverb);
		
			print "<table align=center width=40%><tr><td><div align=center class='continue'><a href='" . $_SERVER['PHP_SELF'] . "'>Continue</a></div>";
			print "</td></tr></table>";
		
		
		
		} elseif (!is_null($_dobj)) {
			// it's a complete io command
			// check 1: are items reachable?
			if ($_allobjs[$_f]->isReachable($_allobjs[$_item]) == FALSE && $_Me->isReachable($_allobjs[$_item]) == FALSE) {
				trigger_error("Out of reach item on object " . $_allobjs[$_item]->name . " (" . $_item . ")");
		
				exit();
			}
			if ($_allobjs[$_f]->isReachable($_allobjs[$_dobj]) == FALSE && $_Me->isReachable($_allobjs[$_dobj]) == FALSE) {
				trigger_error("Out of reach item on object " . $_allobjs[$_dobj]->name . " (" . $_dobj . ")");
		
				exit();
			}
		
			// check 2: is verb verified?
		
			if ($_allobjs[$_dobj]->doVerbVerify($_verb) == FALSE) {
				trigger_error("Unverified doVerb " . $_verb . " on object " . $_allobjs[$_dobj]->name . " (" . $_dobj . ")");
		
				exit();
			}
			if ($_allobjs[$_item]->ioVerbVerify($_verb, $_allobjs[$_dobj]) == FALSE) {
				trigger_error("Unverified ioVerb " . $_verb . " on object " . $_allobjs[$_item]->name . " (" . $_item . ")");
		
				exit();
			}
			
			// TODO with returns
			$_allobjs[$_item]->ioVerbHandle($_verb, $_allobjs[$_dobj]);
			$_f = dynamicString($_allobjs['_interlude']);
		
			$_f .= dynamicString($_Game->turnEnd($_verb, $_item, $_dobj));
			print $_f;
		
// 			if ($_allobjs['_refresh'] && !$_allobjs['_pause'] && strlen($_f) == 0) {
// 				print "<head><meta http-equiv='refresh' content='0; URL=" . $_SERVER['PHP_SELF'] . "'></head>";
// 				print "<div class='continue'><a href='" . $_SERVER['PHP_SELF'] . "'>Redirect</a></div>";
// 			}
// 			print "<table align=center width=40%><tr><td><div align=center class='continue'><a href='" . $_SERVER['PHP_SELF'] . "'>Continue</a></div>";
// 			print "</td></tr></table>";
			if (isset($_allobjs['_refresh']) && !isset($_allobjs['_pause']) && strlen($_f) == 0) {
				//print "<head><meta http-equiv='refresh' content='0; URL=" . $_SERVER['PHP_SELF'] . "'></head>";
				//print "<div class='continue'><a href='" . $_SERVER['PHP_SELF'] . "'>Redirect</a></div>";
			
				$result = array('refresh' => '',
						'location' => $_allobjs['_Me']->location
				);
			}
			$result['continue'] = array('dest' => $_SERVER['PHP_SELF']);
			//print "<table align=center width=30%><tr><td><div align=center class='continue'><a href='" . $_SERVER['PHP_SELF'] . "'>Continue</a></div>";
			//print "</td></tr></table>";

		
		} elseif (!is_null($_verb) && $_verb != "Wait") {
			// process a command (go, ...)
		
			// check 1: is item reachable?
			if ( ($_allobjs[$_f]->isReachable($_allobjs[$_item]) == FALSE && $_Me->isReachable($_allobjs[$_item]) == FALSE) && $_verb != "go") { // go verbs do not have to be reachable.  the dest-oking is handed by verify
				trigger_error("Out of reach item on object " . $_allobjs[$_item]->name . " (" . $_item . ")");
		
				exit();
			}
			// check 2: is verb verified?
			if ($_allobjs[$_item]->doVerbVerify($_verb) == FALSE) {
				trigger_error("Unverified verb " . $_verb . " on object " . $_allobjs[$_item]->name . " (" . $_item . ")");
		
				exit();
			}
			// execute verb
		
			$result = array_merge($result, $_allobjs[$_item]->doVerbHandle($_verb));
			$_f = dynamicString($_allobjs['_interlude']);
		
			$_f .= dynamicString($_Game->turnEnd($_verb, $_item, $_dobj));
			print $_f;
		
			if (isset($_allobjs['_refresh']) && !isset($_allobjs['_pause']) && strlen($_f) == 0) {		
				//print "<head><meta http-equiv='refresh' content='0; URL=" . $_SERVER['PHP_SELF'] . "'></head>";
				//print "<div class='continue'><a href='" . $_SERVER['PHP_SELF'] . "'>Redirect</a></div>";
				
				$result = array('refresh' => '',
						'location' => $_allobjs['_Me']->location
				);
			}
			$result['continue'] = array('dest' => $_SERVER['PHP_SELF']);
			//print "<table align=center width=30%><tr><td><div align=center class='continue'><a href='" . $_SERVER['PHP_SELF'] . "'>Continue</a></div>";
			//print "</td></tr></table>";
		
		} else {
			// no explicit verb.  Find my location and show it
			unset($_verb); // in case of Wait
			$_verb=null;
			//dump('look');
			
			$result = array_merge($result, $_allobjs[$_f]->doVerbHandle("look"));
			//dump($result);
			print dynamicString($_allobjs['_interlude']);
			
			print dynamicString($_Game->turnEnd($_verb, $_item, $_dobj));
		}
		
		$this->prepareFooter($result);
				
		//dump($result);
		
		return $result;
		// persist session
		
	}
	
	function handleSysVerb($verb) {
		global $_allobjs;
		switch ($verb) {
			case 'init':
				$f = $this->init();
				print "<div class='descpane'>" . dynamicString($f) . "</div>";
				break;
			case 'Quit':
				if ($_allobjs['_Game']->quitMsg() == false) return;
				$_allobjs['_Game']->endGame();
			case 'Restore':
				print "<div class='descpane'>";
				print "<form enctype='multipart/form-data' action='". BaseURL() . "_sysverb=truerestore' method='POST'>";
				// print "<input type='hidden" name="MAX_FILE_SIZE" value="30000">
				print "Select the file to restore from: <input name='savefile' type='file'> ";
				print "<input type='submit' value='Restore'>";
				print "</form></div>";
				break;
			case 'About':
				// Display game and system about info
				print "<div class='descpane'><h3>You are playing " . dynamicString($_allobjs['_Game']->Name()) . "</h3>";
				print dynamicString($_allobjs['_Game']->Desc());
				print "</div><div class='descpane'><h3>This game uses PHPAdventure! version 1.1</h3>";
				print "<a href='http://phpadventure.sf.net'>PHPAdventure!</a> is copyright (c) 2003 <a href='mailto:steven777400@users.sourceforge.net'>Steven Kollmansberger</a> and is released under the <a href='http://www.gnu.org/licenses/gpl.html'>GNU General Public License</a>.  ";
				print "<br><br>PHPAdventure! is designed to bridge the gap between interactive fiction and the web while leaving behind the numerous problems associated with a parser.  Adventure games should be easy to write and fun to play, and that's what PHPAdventure! hopes to do.";
				print "<br><br>Many aspects of PHPAdventure! were based on TADS, the text adventure development system.";
				print "</div>";
				break;
			case 'Restart':
				//$_SESSION = array();
				//session_destroy();
				$this->container->get('session')->remove('vars');
				print "<head><meta http-equiv='refresh' content='0; URL=" . $_SERVER['PHP_SELF'] . "'></head>";
				print "<div class='continue'><a href='" . $_SERVER['PHP_SELF'] . "'>Redirect</a></div></td></tr></table>";
				exit();
		}
	}
	
	public function savesession($session)
	{
		global $_allobjs;
		
		unset($_f);
		
		// 		unset($_misc);
		// 		unset($_stage);
		// 		unset($_verb);
		// 		unset($_sysverb);
		// 		unset($_item);
		// 		unset($_dobj);
		// 		unset($_allobjs);
		// 		unset($_mygamefile);
		//$_f = get_defined_vars();
		// 		unset($_f['_SESSION']);
		// 		unset($_f['HTTP_SESSION_VARS']);
		// 		unset($_f['vars']);
		//$session->set('vars', $_f['_allobjs']);
		$session->set('vars', $_allobjs);
		//$_SESSION['vars'] = serialize($_f);
	}
}