<?php
namespace AppBundle\Routing;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class ExtraLoader extends Loader
{
	private $loaded = false;

	public function load($resource, $type = null)
	{
		if (true === $this->loaded) {
			throw new \RuntimeException('Do not add the "extra" loader twice');
		}

		$routes = new RouteCollection();

		// prepare a new route
		$path = '/extra/{parameter}';
		$defaults = array(
				'_controller' => 'AppBundle:Restgame:extra',
		);
		$requirements = array(
				'parameter' => ".*",
		);
 		$route = new Route($path, $defaults, $requirements);
 		//$route = new Route($path, $defaults, array(), array(), '', array(), array('GET'));
 			
		// add the new route to the route collection
		$routeName = 'extraRoute';
		$routes->add($routeName, $route);

		$this->loaded = true;

		return $routes;
	}

	public function supports($resource, $type = null)
	{
		return 'extra' === $type;
	}
}