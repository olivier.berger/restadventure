<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
// use Symfony\Component\Form\Form;
// use FOS\RestBundle\View\View;
// use FOS\RestBundle\Controller\Annotations\QueryParam;
// use FOS\RestBundle\Request\ParamFetcherInterface;
// use Liip\HelloBundle\Document\Article;
// use Liip\HelloBundle\Form\ArticleType;
// use Liip\HelloBundle\Response as HelloResponse;
// use Nelmio\ApiDocBundle\Annotation\ApiDoc;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
//use Symfony\Component\HttpFoundation\Response;

use FOS\RestBundle\Controller\FOSRestController;

use AppBundle\Model\GoldenSkullGame;

//class RestgameController extends Controller 
class RestgameController extends FOSRestController 
{
	/**
	 * @ Route("/", name="start-room")
	 */
// 	public function indexAction()
// 	{
// 		dump($this);
// 		return new Response();
// 	}
	
	// cf. ExtraLoader for routes
	
	public function extraAction(Request $request, $parameter)
	{
 		//dump($parameter);
		
		
		$goldenskullgame = new GoldenSkullGame($this->container);
		//dump($goldenskullgame);
		
		global $_allobjs;
		//dump($_allobjs);
		
		// récupérer _verb, _item dans la requête
		$_sysverb =  $request->query->get('_sysverb');
		$_verb =  $request->query->get('_verb');
		$_item = $request->query->get('_item');
		$_dobj = $request->query->get('_dobj');
		$_misc = $request->query->get('_misc');
		if(!$_item && $parameter) $_item=$parameter;
		if ($_verb) $_sysverb='';
		
		$results = $goldenskullgame->play($_sysverb, $_verb, $_item, $_dobj, $_misc);
		
 		//dump($_allobjs);
		
 		$goldenskullgame->savesession($this->get('session'));
		
// 		return new Response($parameter);

 		if( array_key_exists('refresh', $results) ){
 			$params = "";
 			if(array_key_exists('location', $results)) {
 				$params = array('parameter' => $results['location']);
 			}
 			return $this->redirectToRoute('extraRoute', $params);
 		}
 		else {
		$data = array( 'results' => $results);
		$view = $this->view($data, 200)
	 		->setTemplate("AppBundle:Game:main.html.twig")
 			->setTemplateVar('results')
		;
		
		return $this->handleView($view);
 		}
	}
}