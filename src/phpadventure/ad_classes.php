<?php
/**** ad_classes.php:  This file contains extra classes that can be used for in game objects and things. ****/

namespace phpadventure\Classes;

/***
	PHPAdventure!   Copyright (C) 2003 Steven Kollmansberger

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
***/


/************
	Darkroom:  A room that requires light to operate in.  The only operation (besides inventory and system verbs)
	that will be allowed is to go "out" which will send the player back wherever they came from
	if they can go there.  If not, they are stuck.
	
	A Darkroom will be illuminated (act a normal room) if any object is present with the property
	emitsLight set to TRUE.  You can set this property of the darkroom to true and it will act as
	a normal room.
	NOTICE:  Light search is applied to the room and to _Me non-recursively.  Light sources inside
	containers (even transparent or open ones) will not count.  This should be changed sometime,
	but I'm too lazy to figure out all the details involved right now.
	NOTICE:  In general, this class is pretty rough.

	New/Updated Properties:
	$emitsLight:  set to true to make this room like a normal room
	$darkname:  The name given to this room when it is dark.  NULL means it retains the original name.
	$darkdesc:  The description given to this when it is dark.
	$outoid: Defines where "out" is.  Automatically set in enterRoom
	$outtitle: Indicates the title for the out link.
	$trapdesc: Shown when in the dark without an exit

	Useful Methods:
	lightIsPresent(): Returns TRUE if the room is illuminated.

**********/

class Darkroom extends Room {
	var $emitsLight;
	var $darkname;
	var $darkdesc;
	var $outoid;
	var $outtitle;
	var $trapdesc;
	function Darkroom() {
		$this->emitsLight = FALSE;
		$this->darkname = "In the dark...";
		$this->darkdesc = "You are surrounded by pitch black darkness, and can't see a thing! ";
		$this->outtitle = "Go back.";
		$this->outoid = "";
		$this->trapdesc = "You are trapped!";
		Room::Room();
	}
	function theName() {
		if ($this->lightIsPresent() || is_null($this->darkname)) return parent::theName();

		return "the dark";
	}
	function emitsLight() {
		return $this->emitsLight;
	}
	function lightIsPresent() {
		// check for the presence of a light source
		global $_allobjs;

		if ($this->emitsLight()) return TRUE;
		foreach ($this->contains as $i) {
			if ($_allobjs[$i]->emitsLight()) return TRUE;
		}
		if ($_allobjs[$_allobjs['_Me']->location]->isIn($this) || $_allobjs['_Me']->location == $this->oid)
			foreach ($_allobjs['_Me']->contains as $i) {
				if ($_allobjs[$i]->emitsLight()) return TRUE;
			}
		return FALSE;

	}
	function Name() {

		if ($this->lightIsPresent() || is_null($this->darkname)) return parent::Name();

		return $this->darkname;
	}
	function doVerbHandle($verb) {
		global $_allobjs;
		switch ($verb) {
		case "look":
			if ($this->lightIsPresent()) return parent::doVerbHandle($verb);
			$a = $this->darkdesc . " ";
			if (array_key_exists($this->outoid, $this->cango)) {
				$a = $a . "<a href='" . BaseURL() . "_item=";
				$a = $a . $this->outoid;
				$a = $a . "&_verb=go";
				$a = $a . "'>";
				$a = $a . $this->outtitle;
				$a = $a . "</a>";
			} else {
				$a = $a . $this->trapdesc;
			}
			print "<div class='descpane'>" . dynamicString($a) . "</div>";
			break;
		default:
			return parent::doVerbHandle($verb);
		}
	}

	function isReachable($dest) {
		if ($this->lightIsPresent()) return parent::isReachable($dest);
		return FALSE;
	}
	function enterRoom(&$origoid) {
		$this->outoid = $origoid;
		return parent::enterRoom($orig);
	}



}


/**************

	Openable:  A container which can be open and closed.
	New/Updated Properties:
		$isopen:  Indicates if the container is currently open. Defaults to false.
		$isTransparent:  If TRUE, items can be seen within the container even when the container is closed.
		$openedMsg: shown when the item is opened.
		$closedMsg: shown when the item is closed

	New/Updated Methods:
		isReachable($dest):  Does not allow access through a closed container.

	New/Updated Verbs:
		open: Opens the container.
		close: Closes the container.

**********/

class Openable extends Container {
	var $isTransparent;
	var $isopen;
	var $openedMsg;
	var $closedMsg;

	function Desc() {
		$a = $this->desc . "  " . ucfirst($this->theName()) . " is " . ($this->isopen ? "open. " : "closed. ");
		$a = $a . "<br>" . $this->listContents();
		return $a;
	}
	function aName() {
		if ($this->isopen) return "an open " . $this->Name(); else return "a closed " . $this->Name();
	}
	function listContents($lookdir = false) {
		if ($this->isopen == FALSE && $this->isTransparent == FALSE) return "";
		return parent::listContents();
	}
	function isReachable($dest) { // overloaded!  closed containers don't have reacable things inside

		if ($this->isopen == FALSE)
			return FALSE;
		else
			return parent::isReachable($dest);
	}
	function Openable() {


		Container::Container();

		array_push($this->doVerbs, "open", "close");
		$this->isopen = FALSE;
		$this->isTransparent = FALSE;
		$this->openedMsg = "Opened.";
		$this->closedMsg = "Closed.";

	}

	function doVerbVerify($verb) {

		if ($verb == "open") {
			return !$this->isopen;
		}
		if ($verb == "close") {
			return $this->isopen;
		}
		return parent::doVerbVerify($verb);
	}

	function ioVerbVerify($verb, $dobj) {
		if ($verb == "putin") {
			if ($this->isopen == FALSE) return FALSE; // otherwise return the parent's opinion
		}
		return parent::ioVerbVerify($verb, $dobj);
	}

	function doVerbHandle($verb) {
		global $_allobjs;
		switch ($verb) {
		case "open":
			$this->isopen = TRUE;
			print "<div class='descpane'>" . dynamicString($this->openedMsg) . " " . $this->listContents() . "</div>";

			break;
		case "close":
			$this->isopen = FALSE;
			print "<div class='descpane'>" . dynamicString($this->closedMsg) . "</div>";

			break;

		default:
			parent::doVerbHandle($verb);
		}
	}


	function verbIng($verb) {
		switch ($verb) {
		case "close":
			return "closing " . $this->theName();
		default:
			return parent::verbIng($verb);
		}
	}


}
/**************

	Switchable:  A carryable which can be switched on and off
	New/Updated Properties:
		$ison: Indicates if the item is switched on.  Defaults to TRUE.
		$switchlight:  Indicates that the item is emits light if it is switched on.  Defaults to FALSE.
		$turnedonMsg: Shown when the switch is turned on
		$turnedoffMsg: Shown when the switch is turned off

	New/Updated Methods:


	New/Updated Verbs:
		turnon: Turns on the switch
		turnoff: Turns off the switch

**********/

class Switchable extends Carryable {
	var $ison;
	var $switchlight;
	var $turnedonMsg;
	var $turnedoffMsg;
	
	function Desc() {
		$a = parent::Desc() . " " . ucfirst($this->theName()) . " is currently switched " . ($this->ison ? "on. " : "off. ");
		return $a;
	}

	function Switchable() {
		Carryable::Carryable();
		$this->ison = TRUE;
		$this->switchlight = FALSE;
		$this->turnedonMsg = "Turned on.";
		$this->turnedoffMsg = "Turned off.";
		array_push($this->doVerbs, "turnon", "turnoff");

	}
	function emitsLight() {
		if ($this->switchlight == FALSE) return parent::emitsLight();
		return $this->ison;
	}
	function doVerbVerify($verb) {

		if ($verb == "turnon") {
			return !$this->ison;
		}
		if ($verb == "turnoff") {
			return $this->ison;
		}
		return parent::doVerbVerify($verb);
	}

	function doVerbHandle($verb) {
		global $_allobjs;
		switch ($verb) {
		case "turnon":
			$this->ison = TRUE;
			print "<div class='descpane'>" . dynamicString($this->turnedonMsg) . "</div>";

			break;
		case "turnoff":
			$this->ison = FALSE;
			print "<div class='descpane'>" . dynamicString($this->turnedoffMsg) . "</div>";

			break;

		default:
			parent::doVerbHandle($verb);
		}
	}


	function verbIng($verb) {
		switch ($verb) {
		case "turnon":
			return "turning on " . $this->theName();
		case "turnoff":
			return "turning off " . $this->theName();
		default:
			return parent::verbIng($verb);
		}
	}
	function verbDo($verb) {
		switch ($verb) {
		case "turnon":
			return "Turn on " . $this->theName();
		case "turnoff":
			return "Turn off " . $this->theName();
		default:
			return parent::verbDo($verb);
		}
	}


}

/**************
	NoEntranceRoom:  not a room at all, but rather an easy way to provide links with the "go" verb
		that never allow people to follow them.  if you want a room that sometimes allows enterance,
		this class is not for you.

	New/Updated Properties:
		$rejectMsg:  contains the message to display if the player tries to go there.


	Overloadable Methods:
		rejectMsg():  overload this if you want dynamic messages.  return the message to be displayed.

************/


class NoEntranceRoom extends Object {

	var $rejectMsg;

	function rejectMsg() { return $this->rejectMsg; }
	function NoEntranceRoom() {
		Object::Object();

		array_push($this->doVerbs, "go");
		$this->cango = array();
		$this->hasseen = FALSE;
		$this->rejectMsg = "You can't go there.";
	}
	function doVerbVerify($verb) {
		global $_allobjs;
		if ($verb == "go") {
			$f = $_allobjs[$_allobjs['_Me']->location];

			if (array_key_exists($this->oid, $f->cango)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}

		return parent::doVerbVerify($verb);
	}

	function doVerbHandle($verb) {
		if ($verb == "go") {
			print "<div class='descpane'>" . dynamicString($this->rejectMsg()) . "</div>";

		} else return parent::doVerbHandle($verb);
	}

	function verbIng($verb) {

		switch ($verb) {
		case "go":
			return "going to " . $this->Name();
		default:
			return parent::verbIng($verb);
		}
	}
}

/**************
	Lockable:  An openable which can be locked and unlocked, optionally requiring a key for one
		or both of these operations.

	New/Updated Properties:
		$isLocked:  Set to TRUE if the object is currently locked.
		$keyObjs:  Contains a list of oids of the possible keys.  Use addKey() to set.
		$keylessLock:  Set to TRUE if the object does not require a key to be locked.
		$keylessUnlock:  Set to TRUE if the object does not require a key to be unlocked.

		Note:  If the lock can be operated in a certain way (lock or unlock) without a key,
			use of a key will not be allowed for that way.

		$lockedMsg: Shown when the lock is locked
		$unlockedMsg:  Shown when the lock is unlocked


	New/Updated Methods:
		addKey($obj):  Adds the specified object to the keyObjs list


	New Verbs:
		lock and unlock (for keyless operation)
		
	can be direct object for
		lockwith and unlockwith


**************/


class Lockable extends Openable {
	var $isLocked;
	var $keyObjs;
	var $keylessLock;
	var $keylessUnlock;
	var $lockedMsg;
	var $unlockedMsg;

	
	function Desc() {
		// don't use parent because it has open/close
		$a = $this->desc . "  " . ucfirst($this->theName()) . " is " . ($this->isopen ? "open. " : ($this->isLocked ? "closed and locked. " : "closed. "));
		$a = $a . "<br>" . $this->listContents();
		return $a;
	}

	function addKey($obj) {
		$this->keyObjs[$obj->oid] = $obj->oid;
	}

	function Lockable() {
		Openable::Openable();
		$this->isLocked = FALSE;
		$this->keylessLock = TRUE;
		$this->keylessUnlock = TRUE;
		$this->keyObjs = array();
		$this->lockedMsg = "Locked.";
		$this->unlockedMsg = "Unlocked.";
		array_push($this->doVerbs, "unlock", "lock");

	}

	function doVerbVerify($verb) {
		switch ($verb) {
		case "unlock":
			return ($this->isLocked && $this->keylessUnlock && !$this->isopen);
		case "lock":
			return (!$this->isLocked && $this->keylessLock && !$this->isopen);

		case "unlockwith":
			return ($this->isLocked && !$this->keylessUnlock && !$this->isopen);
		case "lockwith":
			return (!$this->isLocked && !$this->keylessLock && !$this->isopen);

		default:
			return parent::doVerbVerify($verb);
		}
	}
	


	function doVerbHandle($verb) {
		global $_allobjs;
		switch ($verb) {
		case "open":
			if ($this->isLocked) {
				print "<div class='descpane'>It's locked.</div>";
			} else {
				parent::doVerbHandle($verb);
			}

			break;
			
		case "lock":
			$this->isLocked = TRUE;
			print "<div class='descpane'>" . dynamicString($this->lockedMsg) . "</div>";
			break;

		case "unlock":
			$this->isLocked = FALSE;
			print "<div class='descpane'>" . dynamicString($this->unlockedMsg) . "</div>";
			break;

		case "lockwith":
		case "unlockwith":
			print $this->showIoObjs($verb);
			break;

		default:
			parent::doVerbHandle($verb);
		}
	}

	function verbIng($verb) {
		switch ($verb) {
		case "unlock":
			return "Unlocking " . $this->theName();
		case "lock":
			return "Locking " . $this->theName();

		case "unlockwith":
			return "Unlocking " . $this->theName() . " with...";
		case "lockwith":
			return "Locking " . $this->theName() . " with...";

		default:
			return parent::verbIng($verb);
		}

	}

	function verbDo($verb) {
		switch ($verb) {
		case "unlock":
			return "Unlock " . $this->theName();
		case "lock":
			return "Lock " . $this->theName();
		case "unlockwith":
			return "Unlock " . $this->theName() . " with...";
		case "lockwith":
			return "Lock " . $this->theName() . " with...";
		default:
			return parent::verbDo($verb);
		}
	}

}

/********
	Door: A method for connecting two (or more) rooms that can impede progress.

	All Lockable properties apply

	New/Updated properties:
	$isLockable:  if TRUE, the user can lock and unlock the door
	$autoOpen:  if TRUE, the user will automatically unlock and open the door, if possible


	New/Updates methods:
	connectRooms(&$room1, &$room2):  connects two rooms with the door. Use this INSTEAD
		of moveInto!
	
	new verbs: go


*********/

class Door extends Lockable {

	var $isLockable;

	var $autoOpen;

	var $destoids;

	

	function listContents($lookdir = false) { return ""; }
	function Door() {
		Lockable::Lockable();
		$this->isLockable = FALSE;
		$this->isLocked = FALSE;
		$this->isopen = FALSE;
		$this->autoOpen = TRUE;
		$this->name = "door";
		$this->isFixed = TRUE;
		$this->destoids = array();
		$this->canContain = FALSE;
		array_push($this->doVerbs, "go");
	}
	function connectRooms(&$room1, &$room2) {
		// do the hack
		$room1->contains[$this->oid] = $this->oid;
		$room2->contains[$this->oid] = $this->oid;
		$this->destoids[$room1->oid] = $room2->oid;
		$this->destoids[$room2->oid] = $room1->oid;
	}
	function doVerbVerify($verb) {

		global $_allobjs;
		switch ($verb) {
		case "go":
			$f = $_allobjs['_Me']->location;

			if (array_key_exists($f, $this->destoids)) {
				return TRUE;
			} else {
				return FALSE;
			}
		case "unlock":
		case "lock":
		case "unlockwith":
		case "lockwith":
			if ($this->isLockable) return parent::doVerbVerify($verb); else return FALSE;

		default:
			return parent::doVerbVerify($verb);
		}
	}
	
	function verbDo($verb) {
		if ($verb == "go") return "Go through " . $this->theName(); else return parent::verbDo($verb);
	}
	function verbIng($verb) {
		if ($verb == "go") return "Going through " . $this->theName(); else return parent::verbIng($verb);
	}

	function doVerbHandle($verb) {
		global $_allobjs;
		// handle go
		$f = $_allobjs['_Me']->location;

		if ($verb == "go") {
			if ($this->isopen == FALSE) {
				if ($this->autoOpen) {
					if ($this->isLocked) {
						if ($this->keylessUnlock) {
							//$this->isLocked = FALSE;
							//$this->isopen = TRUE;
							print "<div class='descpane'>Unlocking and opening the door...</div>";
							$this->doVerbHandle("unlock");
							$this->doVerbHandle("open");
							$_allobjs['_pause'] = TRUE;
						} else {
							print "<div class='descpane'>The door is locked.</div>";
							return;
						}
					
					} else {
						//$this->isopen = TRUE;
						print "<div class='descpane'>Opening the door...</div>";
						$this->doVerbHandle("open");
						$_allobjs['_pause'] = TRUE;
					}

				} else {
					print "<div class='descpane'>The door is closed.</div>";
					return;
				}
			}

			$a = $_allobjs[$this->destoids[$f]];
			return $a->doVerbHandle("go");


		} else return parent::doVerbHandle($verb);

	}




}

/**************
	MessageFuse: A daemon which automatically which selects a message from
		a given set either at random or sequentially and displays it, one messages
		per turn.

	New/Updated Properties:
		$Messages : an array containing the messages to display.  Use blank strings for no message.
		$Loop : indicate how the messages should be played. TRUE means the messages are played
			sequentially and looped.  FALSE means the messages are played sequentially,
			but the fuse disables once they have all been played.  NULL means the messages
			are played randomly.

************/

class MessageFuse extends Fuse {
	var $Messages;
	var $Loop;

	var $currmsg;

	
	function MessageFuse() {
		Fuse::Fuse();
		$this->Messages = array();
		$this->Loop = FALSE;
		$this->currmsg = 0;
	}

	function fuseProcess($verb, $itemoid, $dobjoid) {
		$this->fuseleft = 1;
		if ($verb == "go") return;
		if (is_null($this->Loop)) {
			// choose a random message
			return $this->Messages[array_rand($this->Messages)];
		} else {
			// play messages in sequence
			$a = array_values($this->Messages);
			$a = $a[$this->currmsg];
			$this->currmsg = $this->currmsg + 1;
			if ($this->currmsg >= count($this->Messages)) {
				$this->currmsg = 0;
				if ($this->Loop == FALSE) $this->fuseleft = 0;
			}
			return $a;
		}
	}


}

/************
	Edible:  Something that the player can eat (or drink)
		By default, nothing special happens when they consume it except that
		the isConsumed flag will be set to true.

	New/Updated Properties:
		$isConsumed:  indicates if the edible has been consumed
		$canEat:  set to true to enable the "eat" verb
		$canDrink: set to true to enable the "drink" verb
			note: eat and drink are handled identically
		$eatMsg: displayed when the player eats it
		$drinkMsg: displayed when the player drinks it

	New Verbs: eat, drink

************/

class Edible extends Carryable {
	var $isConsumed;
	var $canEat;
	var $canDrink;
	var $eatMsg;
	var $drinkMsg;
	function Edible() {
		Carryable::Carryable();
		$this->isConsumed = FALSE;
		$this->canEat = TRUE;
		$this->canDrink = FALSE;
		$this->eatMsg = "Eaten.";
		$this->drinkMsg = "Drunk.";

		array_push($this->doVerbs, "eat", "drink");
	}

	function doVerbVerify($verb) {
		if ($verb == "eat") return $this->canEat && !$this->isConsumed;
		if ($verb == "drink") return $this->canDrink && !$this->isConsumed;
		return parent::doVerbVerify($verb);
	}
	function doVerbHandle($verb) {
		if ($verb == "eat" || $verb == "drink") {
			$this->isConsumed = TRUE;
			$this->moveOut(); // get it out of here, duh
			print "<div class='descpane'>" . ($verb == "eat" ? $this->eatMsg : $this->drinkMsg) . "</div>";
		} else parent::doVerbHandle($verb);
	}
	// don't need formatting because all the defaults will be fine
}


/*********
	Wearable: something the player can wear
	
	New/Updated Properties:
		$isWorn: Indicates if the clothing item is being worn
		$wearMsg:  Shown when the user puts on the item
		$unwearMsg:  Shown when the user takes off the item
		
	New Verbs:  wear, unwear
	
*********/

class Wearable extends Carryable {
	var $isWorn;
	var $wearMsg;
	var $unwearMsg;
	
	/* function Name() {
		return parent::Name() . ($this->isWorn ? " (being worn)" : "");
	} */
	function Desc() {
		return parent::Desc() . ($this->isWorn ? " You are currently wearing " . $this->theName() . "." : "");
	}

	function Wearable() {
		Carryable::Carryable();
		$this->isWorn = FALSE;
		$this->wearMsg = NULL;
		$this->unwearMsg = NULL;
		array_push($this->doVerbs, "wear", "unwear");
	}
	function doVerbVerify($verb) {
		global $_allobjs;
		if ($verb == "wear") {
			if ( ($this->isIn($_allobjs['_Me']) || $this->doVerbVerify("take")) && !$this->isWorn) return TRUE;
			else return FALSE;
		}
		elseif ($verb == "unwear") return $this->isWorn;
		else return parent::doVerbVerify($verb);

	}
	/* function moveInto(&$dest) {
		// normally this should not be overloaded!
		// but we need to take it off if they drop it, etc
		global $_allobjs;
		if ($this->isWorn && $dest->oid != "_Me") {
			$_allobjs['_interlude'] .= "<div class='descpane'>Taking off " . $this->theName() . " first...</div>";
		}
		return parent::moveInto($dest);
	} */

	function doVerbHandle($verb) {
		global $_allobjs;
		if ($verb == "wear") {
			if ($this->isIn($_allobjs['_Me']) == FALSE) {
				print "<div class='descpane'>Taking " . $this->theName() . " first...</div>";

				$this->doVerbHandle("take"); // take veried in wear verify
			}
			if ($this->isIn($_allobjs['_Me']) == FALSE) return; // we didn't succeed in taking it!
			$this->isWorn = TRUE;
			if (is_null($this->wearMsg)) {
				print "<div class='descpane'>You put " . $this->theName() . " on.</div>";
			} else {
				print "<div class='descpane'>" . $this->wearMsg . "</div>";
			}
		}
		elseif ($verb == "unwear") {

			$this->isWorn = FALSE;
			if (is_null($this->unwearMsg)) {
				print "<div class='descpane'>You take off " . $this->theName() . ".</div>";
			} else {
				print "<div class='descpane'>" . $this->unwearMsg . "</div>";
			}
		} else {
			if ($verb != "look" && $this->isWorn) {

				//print "<div class='descpane'>Taking off " . $this->theName() . " first...</div>";
				$this->doVerbHandle("unwear");
				//$this->isWorn = FALSE;
			}
			return parent::doVerbHandle($verb);
		}
	}
	
	function verbIng($verb) {
		switch ($verb) {
		case "wear":
			return "putting on " . $this->theName();
		case "unwear":
			return "taking off " . $this->theName();
		default:
			return parent::verbIng($verb);
		}
	}
	
	function verbDo($verb) {
		switch ($verb) {
		case "wear":
			return "Put on " . $this->theName();
		case "unwear":
			return "Take off " . $this->theName();
		default:
			return parent::verbDo($verb);
		}
	}
}

/********
	SitLayItem:  A thing the player can sit or lay down on, such as a chair, bed,
		stool or even floor.  Also acts as a surface.
		NOTICE:  When in the item, it will be considered as a different room, thus NPCs
			and fuses relying on withMe will stop running.

	New/Updated Properties:
	$isSit:  If TRUE, the player can sit on the item.  If FALSE, they can lay on it instead.
	$inMsg: Shown when the player sits or lays on the item
	$outMsg:  Shown when the player stands up.
	$canSitLay:  If FALSE, SitLay functionality is disabled
	
	Overloadable Methods:
	enterItem():  This function is called when the player sits or lays on the item.
		You can return FALSE to cancel the player's action.
	leaveItem():  This function is called when the player gets up.
		You can return FALSE to cancel the player's action.

	New verbs:  in, out
********/

class SitLayItem extends Surface {
	var $isSit;
	var $canSitLay;
	
	var $cango;

	function statusBar() {
		global $_allobjs;
		return $_allobjs[$this->location]->Name() . ", " . ($this->isSit ? "sitting" : "lying") . " on " . $this->theName();
	}
	function Desc() {
		// we want to know if this is called because of the room, or specifically
		global $_allobjs;
		if (is_null($_allobjs['_verb'])) return $_allobjs[$this->location]->Desc();
		// otherwise do whatever we'd normally do
		return parent::Desc();
	}
	function SitLayItem() {
		Surface::Surface();
		array_push($this->doVerbs, "in", "out");
		$this->isSit = TRUE;
		$this->canSitLay = TRUE;

		$this->isMsg = NULL;
		$this->outMsg = "You stand up.";
		$this->isFixed = TRUE;
		$this->cango = array(); // must be defined for all places a player may be
	}
	/* function listContents($lookdir = FALSE) {
		if (!$this->enableSurface) return "";
		return parent::listContents($lookdir);
	}
	function ioVerbVerify($verb, $dobj) {
		if ($verb == "putin" && !$this->enableSurface) return FALSE;
		return parent::ioVerbVerify($verb, $dobj);
	} */
	function lightIsPresent() { 
		global $_allobjs;
		return $_allobjs[$this->location]->lightIsPresent(); // get from room
	}
	function doVerbVerify($verb) {

		global $_allobjs;
		switch ($verb) {
		case "in":
			return (!($_allobjs['_Me']->location == $this->oid) && $this->canSitLay);
		case "out":
			return (($_allobjs['_Me']->location == $this->oid) && $this->canSitLay);

		default:
			return parent::doVerbVerify($verb);
		}
	}
	
	function verbDo($verb) {
		switch ($verb) {
		case "in":
			return ($this->isSit ? "Sit " : "Lay ")  . "on " . $this->theName();
		case "out":
			return "Stand up";
		default:
			return parent::verbDo($verb);
		}
	}

	function verbIng($verb) {
		switch ($verb) {
		case "in":
			return ($this->isSit ? "sitting " : "lying ")  . "on " . $this->theName();
		case "out":
			return "standing up";
		default:
			return parent::verbIng($verb);
		}
	}

	function doVerbHandle($verb) {
		global $_allobjs;
		switch ($verb) {
		case "in":
			if ($this->enterItem() == FALSE) return;
			$_allobjs['_Me']->moveInto($this);
			if (is_null($this->inMsg)) {
				print "<div class='descpane'>You " . ($this->isSit ? "sit " : "lay ")  . "on " . $this->theName() . ".</div>";
			} else {
				print "<div class='descpane'>" . dynamicString($this->inMsg) . "</div>";
			}
			break;
		case "out":
			if ($this->leaveItem() == FALSE) return;
			$_allobjs['_Me']->moveInto($_allobjs[$this->location]);
			print "<div class='descpane'>" . dynamicString($this->outMsg) . "</div>";
			break;
		default:
			parent::doVerbHandle($verb);

		}
	}
	
	function enterItem() {
			// default does nothing
			return TRUE;
	}

	function leaveItem() {
			// default does nothing
			return TRUE;
	}
}

/**************

	Button:  A carryable which can be "pushed".  Does not toggle.

	New/Updated Properties:
		$pushMsg:  Displayed when the button is pushed

	New/Updated Verbs:
		push

**********/

class Button extends Carryable {
	var $pushMsg;

	function Button() {
		Carryable::Carryable();
		$this->isFixed = TRUE;
		array_push($this->doVerbs, "push");

	}
	
	function doVerbVerify($verb) {

		if ($verb == "push") {
			return TRUE;
		}

		return parent::doVerbVerify($verb);
	}

	function doVerbHandle($verb) {
		global $_allobjs;
		switch ($verb) {
		case "push":

			print "<div class='descpane'>" . dynamicString($this->pushedMsg) . "</div>";

			break;

		default:
			parent::doVerbHandle($verb);
		}
	}



}


/**** end of ad_classes.php ****/
?>
