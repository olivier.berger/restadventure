<?php

/***
 PHPAdventure!   Copyright (C) 2003 Steven Kollmansberger

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 ***/

namespace phpadventure;

/*************
 Carryable:  An object which can be picked up and dropped, and carried around in the player's inventory
 Note that almost all types of game objects are derived from Carryable.  Obviously, you will not
 want the player to be able to pick up all these objects, so you can turn off the Carryable functionality
 in a given object.

 New/Updated Properties:
 $isFixed: set this property to true to disable the Carryable features in a given object.
 $isListed: as before, except that it defaults to NULL.  When NULL, the object is listed if not fixed, and not listed if fixed.
 A particular true/false value here overrides this.
 $isKey:  should this item be listed as a possible key (usually leave as TRUE).
 $takenMsg: shown when the user takes it.
 $droppedMsg: shown when the user drops it.

 New/Updated Verbs:
 take: moves the item into the inventory
 drop: moves the item to the player's current location

 Can be Direct Object for:
 putin



 **********/



class Carryable extends Object {
	// this kind will allow to be taken and dropped

	var $isFixed; // set to true to disable carryable features
	var $isKey;
	var $takenMsg;
	var $droppedMsg;

	function isListed() {

		if (is_null($this->isListed)) return !$this->isFixed;
		return $this->isListed;
	}
	function __construct() {


		parent::__construct();

		$this->isFixed = FALSE;
		$this->isListed = NULL; // now null
		$this->isKey = TRUE;
		$this->takenMsg = "Taken.";
		$this->droppedMsg = "Dropped.";
		array_push($this->doVerbs, "take", "drop");
		array_push($this->ioVerbs, "lockwith", "unlockwith");
	}

	function doVerbVerify($verb) {
		global $_allobjs;
		if ($verb == "take") {
			return (!$this->isIn($_allobjs['_Me']) && !$this->isFixed);
		}
		if ($verb == "drop") {
			return ($this->isIn($_allobjs['_Me']) && !$this->isFixed);
		}
		if ($verb == "putin") {
			// this occurs when this is the dobj to a putin
			return !$this->isFixed;
		}

		return parent::doVerbVerify($verb);
	}

	function doVerbHandle($verb) {
		$result = array();
		global $_allobjs;
		switch ($verb) {
			case "take":
				$me =& $_allobjs['_Me'];
				$this->moveInto($me);
				print "<div class='descpane'>" . dynamicString($this->takenMsg) . "</div>";

				break;
			case "drop":
				$me =& $_allobjs['_Me'];
				$ml =& $_allobjs[$me->location];
				$this->moveInto($ml);
				print "<div class='descpane'>" . dynamicString($this->droppedMsg) . "</div>";

				break;
			case "putin":

				// the dobj will be the first to receive the command
				print $this->showIoObjs($verb);
				break;

			default:
				return parent::doVerbHandle($verb);
		}
		return $result;
	}

	function verbIng($verb) {
		switch ($verb) {
			case "take":
				return "taking " . $this->theName();
			case "drop":
				return "dropping " . $this->theName();
			case "putin":
				return "putting " . $this->theName() . " in/on...";

			default:
				return parent::verbIng($verb);
		}
	}

	function verbDo($verb) {
		switch ($verb) {
			case "putin":
				return "Put " . $this->theName() . " in/on...";

			default:
				return parent::verbDo($verb);
		}
	}

	function verbIoIng($verb, $dobj) {
		switch ($verb) {
			case "lockwith":
				return "Locking " . $dobj->theName() . " with " . $this->theName();
			case "unlockwith":
				return "Unlocking " . $dobj->theName() . " with " . $this->theName();
			default:
				return parent::verbIoIng($verb, $dobj);
		}
	}

	function verbIo($verb, $dobj) {
		global $_allobjs;
		switch ($verb) {
			case "lockwith":
				return "Lock " . $dobj->theName() . " with " . $this->theName();
			case "unlockwith":
				return "Unlock " . $dobj->theName() . " with " . $this->theName();
			default:
				return parent::verbIo($verb, $dobj);
		}
	}

	function ioVerbVerify($verb, $dobj) {
		switch ($verb) {
			case "unlockwith":
			case "lockwith":
				return !$this->isFixed && $this->isKey;
			default:
				return parent::ioVerbVerify($verb, $dobj);
		}
	}

	function ioVerbHandle($verb, &$dobj) {
		switch ($verb) {
			case "unlockwith":
				if (array_key_exists($this->oid, $dobj->keyObjs)) {
					$dobj->isLocked = FALSE;
					print "<div class='descpane'>" . dynamicString($dobj->lockedMsg) . "</div>";
				} else {
					print "<div class='descpane'>" . ucfirst(dynamicString($this->theName())) . " doesn't fit the lock.</div>";
				}
				break;
			case "lockwith":
				if (array_key_exists($this->oid, $dobj->keyObjs)) {
					$dobj->isLocked = TRUE;
					print "<div class='descpane'>" . dynamicString($dobj->unlockedMsg) . "</div>";
				} else {
					print "<div class='descpane'>" . ucfirst(dynamicString($this->theName())) . " doesn't fit the lock.</div>";
				}
				break;
			default:
				return parent::ioVerbHandle($verb, $dobj);
		}

	}
	
	function defaultVerb() {
		return "look";
	}

}
