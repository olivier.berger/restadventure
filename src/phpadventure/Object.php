<?php

/***
 PHPAdventure!   Copyright (C) 2003 Steven Kollmansberger

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 ***/

namespace phpadventure;


/**********
 Object:  The most basic kind of thing in the game.  Objects are by default non-interactive, non-listed.

 Properties:
 $name: the short name of the object (always accessed via Name())
 $desc: the longer description of the object (shown when the object is looked at, always accessed via Desc())
 $isListed: indicates if the object should be listed in a room or container description (always accessed via isListed())
 $doVerbs: indicates which verbs are recognized by this object (note: change only in derived class, see below)
 $ioVerbs: indicates which verbs are recognized by this object (note: change only in derived class, see below)
 $linkTitle: indicates the link title.  Use NULL to default to using the Desc()
 $location: indicates the location, if the object is in one location.  Do not set this, use moveInto!!
 $contains: an array of oids inside this object.  Do not set this, use moveInto!!
 Note: Usually, you should use function isIn to recursively check this.
 $attachcontains: an array of oids of things "attached" to this object.  Whenever the object
 moves, the attached items will move as well.  Use attachTo to set this.
 $aname: overrides the default aName if specified
 $thename: overrides the default theName if specified
 $invListed:  defaults to TRUE.  Indicates if the player has this item in their top-level inventory,
 should it be listed?

 Overloadable Methods:
 Name(): the short name of the object
 aName(): the indefinte version of the object short name
 theName(): the definite version of the object short name
 Desc(): the longer description of the object
 isListed(): indicates if the object should be listed in a room or container description
 doVerbVerify($verb): indicates if a given verb is acceptable to be executed with this object as the direct object.
 ioVerbVerify($verb, $dobj): indicates if a given verb is acceptable to be executed.  This object will be the indirect object.
 verbIng($verb): returns the -"ing" form of the given verb.  For the status bar.
 verbIoIng($verb, $dobj): returns the -"ing" form of the given verb.  For the status bar.
 verbDo($verb): return the command link text for a given verb with this object as the direct object
 verbIo($verb, $dobj): return the command link text for a given verb this this object as the indirect object
 doVerbHandle($verb): executes the specified verb with this object as the direct object
 ioVerbHandle($verb, &$dobj): executes the specified verb with this object as the indirect object
 contentsChanged($entered, &$obj):  Called when an object is added or removed from the contents
 entered will be TRUE if the object is added or FALSE otherwise.  obj will be the object in question.
 emitsLight(): returns TRUE if the object emits light or FALSE otherwise.
 linkTitle(): returns a string for the link tooltip.  Do not use ' or HTML
 statusBar():  returns the base string to appear in the status bar.  Only useful for places the player will be in.

 Other Useful Methods:
 detach($full = FALSE):  detachs (cancels any cases where this is attached to another object).
 if $full is set to TRUE, also cancels all objects attached to this object.
 attachTo(&$dest, $bothsides = FALSE): attaches this object to another object.
 Note that you cannot put anything
 with attached objects into multiple locations (e.g. attachTo and moveIntoMulti
 are incompatible).  If $bothsides is set to TRUE, the attachment is done both
 ways (this is useful if neither object is fixed).  Note that to detach, you must
 detach both sides, or use a full detach.  Attachment simply means that if A is
 attached to B, wherever B goes, A will be in the same place (not in B, but in
 B's location).  You can make subcomponents by turning their isListed and invListed
 to FALSE.
 moveInto(&$dest): moves the object into the specified room or container.
 moveIntoMulti($destarray): specifies an array of destinations to put the item in. location will be null
 moveOut(): moves the object into la-la land
 isReachable($dest): indicates if the specified destination can be reached from this object
 isIn($dest): indicates if the specified destination is contained (perhaps at a deep level) within this object
 objRef($verb = "look", $title = NULL, $dobj = NULL): Generates a link to an object if the verb verifies AND the object is reachable.
 If the object verb does not verify or the object is not reachable, only the title is displayed.  No link is created.  The default title is the object's aName()
 Note that when adding links to fixed display objects, they will not be reachable.  Do not use this function except in another function.
 fixedObjRef($verb = "look", $title = NULL, $dobj = NULL): As above, but does not check reachability when generating
 links.  Note that reachability will be checked when the link is clicked.
 withMe(): returns true if the object is either in the player's possession OR in the same room as the player

 Verbs:
 look: Displays the description

 Derived Objects:
 Start with the base new object framework
 1)	If a constructor is needed, be sure to call the parent class constructor.
 i.e. if you have MyObject extends OldObject, call OldObject::OldObject() in the MyObject constructor
 2)	If you want to add a direct object verb, complete the following:
 a) In the constructor, execute array_push($doVerbs, "mydoverb");  this will add the verb as recognized by this object
 b) In doVerbVerify, add a condition to return TRUE when the verb should be allowed.  Do not make any state changes
 in this routine.
 c) In doVerbHandle, add a routine to process the request.  Note that doVerbVerify is always checked before doVerbHandle is called,
 so you do not need to duplicate the checking code.
 d) If the default phrase "verbing the object" doesn't work for your verb, add the appropriate entry into verbIng
 e) If the defulat phrase "verb the object" doesn't work for your verb, add the appropriate entry into verbDo
 3)	If you want to add an indirect object verb, complete the following:
 This would be put <x> in <y> kind of thing.  In that case, x would be the class(es) that could be the
 direct object and y would be the classes that could be the indirect object.
 You need to change both classes to support this.
 a) For the object class(es) which will be the direct object:
 1) In the constructor, execute array_push($doVerbs, "myverb");  this will add the verb as recognized by this object
 2) In doVerbVerify, add a condition to return TRUE when the verb should be allowed.  Do not make any state changes
 in this routine.
 3) In doVerbHandle, add the following command for the verb: print $this->showIoObjs("myverb");
 4) Modify verbIng and verbDo appropriately.  Indirect object verbs should have "..." appended to the end
 of both the verbIng phrase and the verbDo phrase.  This helps users recognize that they
 will need to select an indirect object.
 b) For the object class(es) which will be the indirect object:
 1) In the constructor, execute array_push($this->ioVerbs, "myverb");  this will add the verb as recognized by the object
 2) In ioVerbVerify, add a condition to return TRUE when the verb should be allowed.  Note that the user has already selected
 a direct object (even if they haven't selected this verb yet!) and that direct object will be supplied.
 3) In ioVerbHandle, add a routine to process the request.  The direct object will be supplied.  As before, both
 doVerbVerify on the direct object and ioVerbVerify on the indirect object have been called, so no
 check duplication is needed.
 4) If the default phrase "myverb direct_object indirect_object" doesn't work for your verb, add the appropriate entry
 into verbIo.



 ***********/
class Object {
	var $location;  // NEVER set this!  ALWAYS use moveInto!
	var $contains; // NEVER set this!  ALWAYS use moveInto!
	var $attachedcontains; // NEVER set this!  ALWAYS use attachTo!
	var $name;
	var $aname;
	var $thename;
	var $desc;
	var $oid;
	var $doVerbs;
	var $ioVerbs;
	var $isListed;
	var $linkTitle;
	var $invListed;

	function Name() { return $this->name; /* . ($this->emitsLight() ? " (illuminating)" : "") */ }
	function statusBar() { return $this->Name(); }
	function aName() {
		if (!is_null($this->aname)) return $this->aname;
		$a = strtoupper($this->Name());
		if ($a[0] == 'A' || $a[0] == 'E' || $a[0] == 'I' || $a[0] == 'O' || $a[0] == 'U') {
			return "an " . $this->Name();
		} else
			return "a " . $this->Name();
	}
	function theName() { if (is_null($this->thename)) return "the " . $this->Name(); else return $this->thename;}
	function Desc() { return $this->desc; }



	function emitsLight() { return FALSE; }

	function linkTitle() {
		if (is_null($this->linkTitle))
			return filterHtml(str_replace("'", "&#039;", dynamicString($this->desc)));
			else
				return $this->linkTitle;
	}

	function __construct() {
		global $_allobjs;

		$this->location = NULL;
		$this->aname = NULL;
		$this->thename = NULL;
		$this->contains = array();
		$this->attachcontains = array();
		$this->doVerbs = array("look");
		$this->ioVerbs = array();
		$this->isListed = FALSE;
		$this->invListed = TRUE;

		//$this->oid = count($_allobjs);
		//$_allobjs[$this->oid] =& $this;

	}
	function isListed() {
		return $this->isListed;
	}
	function objRef($verb = "look", $title = NULL, $dobj = NULL, $misc = NULL) {
		$result = array();
		global $_allobjs;
		$ml = $_allobjs['_Me']->location;
		$f = FALSE;


		// huge conjuction - check to see if all verbs verify for both normal and io types!!


		if (( $this->doVerbVerify($verb) || !is_null($dobj) ) &&
				($_allobjs['_Me']->isReachable($this) || $_allobjs[$ml]->isReachable($this) || $verb == "go")
				&& ( is_null($dobj) || $_allobjs['_Me']->isReachable($dobj) || $_allobjs[$ml]->isReachable($dobj) )
				&& ( is_null($dobj) || ($this->ioVerbVerify($verb, $dobj) && $dobj->doVerbVerify($verb))  ) ) {

					// whew!
					//$a = "<a href='" . BaseURL() . "_item=";

					//$a = $a . $this->oid;
					$result['item'] = $this->oid;
					//$a = $a . "&_verb=" . $verb;
					$result['verb'] = $verb;
					if (!is_null($dobj)) {
						//$a = $a . "&_dobj=" . $dobj->oid;
						$result['dobj'] = $dobj->oid;
					}
					if (!is_null($misc)) {
						//$a = $a . "&_misc=" . $misc;
						$result['misc'] = $misc;
					}
					//$a = $a . "'";
					if ($verb == "look") {
						//$a = $a . "title='" . $this->linkTitle() . "'";
						$result['title'] = $this->linkTitle();
					}
					//$a = $a . ">";
					//$f = TRUE;
				}
				if (is_null($title)) {
					//$a = $a . $this->Name();
					$result['message'] = $this->Name();
				} else {
					//$a = $a . $title;
					$result['message'] = $title;
				}
// 				if ($f) {
// 					$a = $a . "</a>";
// 				}
				return $result;
	}
	function htmlLinkObjRef($objref) {
		$a = "<a href='" . BaseURL() . "_item=";
		
		$a = $a . $objref['item'];
		$a = $a . "&_verb=" . $objref['verb'];
		if (array_key_exists('dobj',$objref)) {
			$a = $a . "&_dobj=" . $objref['dobj'];
		}
		if (array_key_exists('misc',$objref)) {
			$a = $a . "&_misc=" . $objref['misc'];
		}
		$a = $a . "'";
		if ($objref['verb'] == "look") {
			$a = $a . "title='" . $objref['title'] . "'";
		}
		$a = $a . ">";
		$f = TRUE;
		
		$a = $a . $objref['message'];
		
						if ($f) {
							$a = $a . "</a>";
						}
						return $a;
	}
	

	function fixedObjRef($verb = "look", $title = NULL, $dobj = NULL) {
		$a =  '<<return $_allobjs[\'' . $this->oid . '\']->objRef(\'' . $verb . '\', ' . (is_null($title) ? 'NULL ' : '\'' . $title . '\'') . ', ' . (is_null($dobj) ? 'NULL' : '\'' . $dobj . '\'') . ');>>';

		return $a;
		/* global $_allobjs;
		 if ($this->doVerbVerify($verb)) {
			$a = "<a href='" . BaseURL() . "_item=";

			$a = $a . $this->oid;
			$a = $a . "&_verb=" . $verb;
			if (!is_null($dobj)) {
			$a = $a . "&_dobj=" . $dobj->oid;
			}
			$a = $a . "'";
			if ($verb == "look") $a = $a . "title='" . $this->linkTitle() . "'";
			$a = $a . ">";

			}
			if (is_null($title)) {
			$a = $a . $this->Name();
			} else {
			$a = $a . $title;
			}
			if ($this->doVerbVerify($verb)) {
			$a = $a . "</a>";
			}
			return $a; */
	}

	function _localIoVerbs($except = NULL, $th) {// th is "this" except that this function is recursive across objects so this is lost
		global $_allobjs;
		$allverbs = array();

		if($this->contains) {
		foreach ($this->contains as $i) {
			$i = $_allobjs[$i];
			if ( ($_allobjs[$_allobjs['_Me']->location]->isReachable($i) || $_allobjs['_Me']->isReachable($i)) && (/*!$_allobjs[$th]->isIn($i) &&*/ $th->oid != $i->oid) ) {
				if($i->ioVerbs) {
				foreach ($i->ioVerbs as $i2) {

					if ($i2 != $except &&  $i->ioVerbVerify($i2, $th) && $i->oid != $th->oid) array_push($allverbs, $i2); // $this->doVerbVerify($i2) &&
				}
				}
				$allverbs = array_merge($allverbs, $i->_localIoVerbs($except, $th));
			}
		}
		}
		return $allverbs;
	}


	function _ioVerbs($except = NULL) {
		global $_allobjs;
		$allverbs = array();

		// ioverbs come from all surfaces that I can reach

		// ioverbs come from me

		$allverbs = array_merge($_allobjs[$_allobjs['_Me']->location]->_localIoVerbs($except, $this), $_allobjs['_Me']->_localIoVerbs($except, $this));


		$allverbs =  array_unique($allverbs);

		$oc = count($allverbs);

		foreach ($allverbs as $i => $j) {

			if ($this->doVerbVerify($allverbs[$i]) == FALSE && !is_null($allverbs[$i])) {

				unset($allverbs[$i]);



			}
		}

		return $allverbs;
	}

	function _localIoObjs($verb, $th) { // th is "this" except that this function is recursive across objects so this is lost
		global $_allobjs;
		$ioobjs = array();

		foreach ($this->contains as $i2) {
			$i =& $_allobjs[$i2];
			$tmp = $_allobjs[$_allobjs['_Me']->location];
			if (($tmp->isReachable($i) || $_allobjs['_Me']->isReachable($i)) && (/*!$_allobjs[$th]->isIn($i) &&*/ $th->oid != $i->oid) ) {

				if ($i->ioVerbVerify($verb, $th) && $th->oid != $i->oid) array_push($ioobjs, $i->oid); // $this->doVerbVerify($verb) &&
				$ioobjs = array_merge($ioobjs, $i->_localIoObjs($verb, $th));
			}

		}
		return $ioobjs;
	}

	function _ioObjs($verb) {
		global $_allobjs;
		$ioobjs = array();

		$ioobjs = array_merge($_allobjs[$_allobjs['_Me']->location]->_localIoObjs($verb, $this), $_allobjs['_Me']->_localIoObjs($verb, $this));

		$ioobjs =  array_unique($ioobjs);
		/*for ($i = 0; $i < count($ioobjs); $i++) {
			if ($this->doVerbVerify($allverbs[$i]) == FALSE) {
			unset($allverbs[$i]);
			$i--;
			}
			}*/
		return $ioobjs;
	}

	function withMe() {
		global $_allobjs;
		return ($this->isin($_allobjs['_Me']) || $this->isin($_allobjs[$_allobjs['_Me']->location]));
	}

	function showIoObjs($verb) {
		// for this as the direct object
		global $_allobjs;
		$ioobjs = $this->_ioObjs($verb);
		asort($ioobjs);

		if (count($ioobjs) == 0) return "";

		$a = "<div class='verbpane'><table align=center cellpadding=5><tr>";
		$icnt = 0;
		foreach ($ioobjs as $i) {
			$i = $_allobjs[$i];
			$icnt = $icnt + 1;
			$a = $a . "<td ";
			if ($icnt % 2 == 0) $a = $a . "align=right";
			if ($icnt % 2 == 1 && $icnt < count($ioobjs)) $a = $a . "align=left";
			$a = $a . ">";
			$a = $a . $i->htmlLinkObjRef($i->objRef($verb, $i->verbIo($verb, $this), $this)) . "</td>";
			if ($icnt % 2 == 0) $a = $a . "</tr><tr>";
		}
		$a = $a . "</tr></table></div>";
		return $a;
	}

	function showVerbs($except = NULL) {
		$result = array();
		//$allverbs = array_merge($this->doVerbs, $this->ioVerbs);

		$allverbs = array();
		if($this->doVerbs) {
		foreach ($this->doVerbs as $i) {

			if ($i != $except && $this->doVerbVerify($i)) array_push($allverbs, $i);
		}
		}
		$allverbs = array_merge($allverbs, $this->_ioVerbs($except));

		asort($allverbs);
		if (count($allverbs) == 0) return $result;
// 		$a = "<div class='verbpane'><table align=center cellpadding=5><tr>";
// 		$icnt = 0;
		foreach ($allverbs as $i) {

// 			$a = $a . "<td ";
// 			$icnt = $icnt + 1;
// 			if ($icnt % 2 == 0) $a = $a . "align=right";
// 			if ($icnt % 2 == 1 && $icnt < count($allverbs)) $a = $a . "align=left";
// 			$a = $a . ">";
// 			$a = $a . $this->objRef($i, $this->verbDo($i)) . "</td>";
// 			if ($icnt % 2 == 0) $a = $a . "</tr><tr>";
			$objref = $this->objRef($i, $this->verbDo($i));
			$result[] = $objref;
 		}
		//$a = $a . "</tr></table></div>";

		return $result;
	}

	function contentsChanged($entered, &$obj) {
		// no behavior by default

	}

	function _moveOut(&$obj) {

		if (isset($this->contains[$obj->oid])) {
			unset($this->contains[$obj->oid]);

			$this->contentsChanged(FALSE, $obj);
		}


	}

	function moveOut() {
		global $_allobjs;
		if (!is_null($this->location) ) {
			$_allobjs[$this->location]->_moveOut($this);
			// need to also move all attached items
			foreach ($this->attachcontains as $k) {
				$_allobjs[$k]->moveOut();
			}
		} else {
			// could be in many locations, must check them all
			foreach ($_allobjs as $k => $i) {
				if (is_a($i, "Object")) $i->_moveOut($this);
			}
		}
		$this->location = NULL;
	}

	function moveInto(&$dest) {
		global $_allobjs;
		$destoid = $dest->oid;



		if (!is_a($dest, "phpadventure\Object")) {
			trigger_error("Invalid destination ".$destoid." for moveInto in " . $this->name . " (". $this->oid . ")");
		}

		$this->moveOut();

		$this->location = $destoid;
		if ($this->oid != '_Me' && !isset($dest->contains[$this->oid])) {// we don't store ourselves inside things
			$dest->contains[$this->oid] = $this->oid;
			$dest->contentsChanged(TRUE, $this);
		}
		// need to also move all attached items
		if($this->attachedcontains) {
			foreach ($this->attachedcontains as $k) {
				$_allobjs[$k]->moveInto($dest);
			}
		}



	}

	function moveIntoMulti($destarray) {
		global $_allobjs;
		$this->moveOut();
		$this->location = NULL;
		foreach ($destarray as $tdest) {
			$dest =& $_allobjs[$tdest->oid];
			$destoid = $dest->oid;
			if (!is_a($dest, "Object")) {
				trigger_error("Invalid destination ".$destoid." for moveInto in " . $this->name . " (". $this->oid . ")");
			}
			if ($this->oid != '_Me' && !isset($dest->contains[$this->oid])) {// we don't store ourselves inside things
				$dest->contains[$this->oid] = $this->oid;
				$dest->contentsChanged(TRUE, $this);
			}
		}

	}
	function detach($full = FALSE) {
		global $_allobjs;
		foreach ($_allobjs as $i) {
			if (isset($i->attachcontains[$this->oid]))
				unset($i->attachcontains[$this->oid]);
		}
		if ($full) $this->attachcontains = array();

	}
	function attachTo(&$dest, $bothsides = FALSE) {
		global $_allobjs;
		if (!is_a($dest, "Object")) {
			trigger_error("Invalid destination ".$destoid." for attachTo in " . $this->name . " (". $this->oid . ")");
		}
		$this->detach();
		$this->moveInto($_allobjs[$_allobjs[$dest->oid]->location]); // get the initial state right
		$_allobjs[$dest->oid]->attachcontains[$this->oid] = $this->oid;

		if ($bothsides) $dest->attachTo($this);


	}

	function isReachable($dest) {
		global $_allobjs;

		if ($dest->oid == $this->oid) return TRUE;
		if(isset($this->contains)) {
		foreach ($this->contains as $i) {
			$f = $_allobjs[$i];
			if($f->oid == $dest->oid) return TRUE;

			if ($f->isReachable($dest)) return TRUE;
		}
		}
		return FALSE;
	}

	function isIn($dest) {
		global $_allobjs;
		if(isset($dest->contains)) {
		foreach ($dest->contains as $i) {
			$f = $_allobjs[$i];
			if ($f->oid == $this->oid) return TRUE;
			if ($this->isIn($f)) return TRUE;
		}
		}
		return FALSE;
	}

	function doVerbVerify($verb) {
		if ($verb == "look") return TRUE;
		return FALSE;
	}

	function ioVerbVerify($verb, $dobj) {

		return FALSE;
	}

	function verbIng($verb) {
		switch ($verb) {
			case "look":
				return "looking at " . $this->theName();
			default:
				return $verb . "ing " . $this->theName();
		}
	}

	function verbIoIng($verb, $dobj) {
		return ucfirst($verb) . "ing " . $dobj->theName() . " -> " . $this->theName();

	}


	function verbDo($verb) {
		switch ($verb) {
			case "look":
				return "Look at " . $this->theName();
			default:
				return ucfirst($verb) . " " . $this->theName();
		}
	}

	function verbIo($verb, $dobj) {
		global $_allobjs;
		return ucfirst($verb) . " " . $dobj->theName() . " -> " .$this->theName();
	}

	function doVerbHandle($verb) {
		$result = array();
		global $_allobjs;
		switch ($verb) {
			case "look":
					
				// Describe the object and show other actions available
				//print "<div class='descpane'>" . dynamicString($this->Desc());
				//print "</div>";
				$result['description'] = $this->Desc();
				//print $_allobjs['_interlude'];
				$result['_interlude'] = $_allobjs['_interlude'];
				$_allobjs['_interlude'] = "";
				//print $this->showVerbs($verb);
				$result['verbs'] = $this->showVerbs($verb);
				break;
			default:
				// Error!
				trigger_error("Unhandled doVerb " . $verb . " in " . $this->name . " (". $this->oid . ")");
		}
		return $result;
	}

	function ioVerbHandle($verb, &$dobj) {
		trigger_error("Unhandled ioVerb " . $verb . " in " . $this->name . " (". $this->oid . ")");
	}

	function defaultVerb() {
		return null;
	}
};
