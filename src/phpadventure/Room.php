<?php

/***
 PHPAdventure!   Copyright (C) 2003 Steven Kollmansberger

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 ***/

namespace phpadventure;

/*************
 Room: An object where the charactor can be.  A room is similiar to a container, but is designed for charectors to be in and travel
 from one room to another.

 New/Updated Properties:
 $cango: lists acceptable destination rooms for travel.  Use addGo to add an entry and output a link at the same time.
 $firstseen:  displayed when the first time a player enters a room
 $linkPref: prefaces an addgo link (to help the user see that the link is to go, not to look)

 New/Updated Overloadable Methods:
 enterRoom(&$origoid):  This function is called when a room is entered.  Any output should be returned as a variable (which will then
 be displayed) so that the system knows to pause.  Non-visible changes do not need to return anything.  You can set
 origoid to NULL to about travel, but other changes will be ignored.
 leaveRoom(&$destoid):  This function is called when a room is left.  Any output should be returned as a variable (which will then
 be displayed) so that the system knows to pause.  Non-visible changes do not need to return anything.
 Note that you can alter the destination or even cancel travel by setting it to NULL.  It's generally
 nice to display a message if you do. :)
 firstSeen():  Called the first time a player enters a room.  Returns text to be displayed.

 New/Updated Useful Methods:
 addGo($destoid, $title): use this function to add a room destination and create a link at the same time.  Note that $dest is NOT the room
 object itself but merely the name of the room.  This is done so that rooms can connect to each other without
 the room needing to exist first (catch-22).  When this function is called, it returns a link with the specified title.
 Calling this function multiple times for one room is no problem.

 New/Updated Verbs:
 look: as before, but does not display any verbs.
 go: moves to another room, if appropriate.




 ***********/

class Room extends Object {
	var $cango;
	var $firstseen;
	var $hasseen;
	var $linkPref;

	function firstSeen() { return $this->firstseen; }
	function aName() { return $this->Name(); } // because rooms aren't a
	/* function theName() { return $this->Name(); } */

	function Desc() {
		$a = parent::Desc() . "<br>" . $this->listContents();
		return $a;
	}
	function addGo($destoid, $title) {
		global $_allobjs;
		if (isset($this->cango) && (array_key_exists($destoid, $this->cango) == FALSE)) {
			$this->cango[$destoid] = $destoid;
		}
		
		 $a = "<a href='" . BaseURL() . "_item=";
		 $a = $a . $destoid;
		 $a = $a . "&_verb=go";
		 $a = $a . "'";

		 $a = $a . ">";
		 $a = $a . $this->linkPref . $title;
		 $a = $a . "</a>";

		//$a =  '<<return $_allobjs[\'' . $destoid . '\']->objRef(\'go\', \'' . $this->linkPref . $title . '\');>>';
		
		
		//$a = $_allobjs[$destoid]->fixedObjRef("go", $this->linkPref . $title);

		return $a;

	}
	function listContents() { // not totally identical to openable listcontents

		global $_allobjs;
		$tmp = array();
		foreach ($this->contains as $k => $i) {
			if ($_allobjs[$i]->isListed()) {
				$tmp[$k] = $i;
			}
		}

		if (count($tmp) == 0) {
			return "";
		}

		$a = "You see ";
		$b = "";
		$c = '';
		foreach ($tmp as $i) {
			if ($c > "") $b = ", " . $b;
			$c = $c . $b;
			$f = $_allobjs[$i];
			if ($f->doVerbVerify("look")) {
				$b = $f->objRef("look", $f->aName());
				$b = $f->htmlLinkObjRef($b);
			}
			else {
				$b = $f->aName();
			}

		}
		$a = $a . $c;
		if ($c > "") $a = $a . " and " ;
		$a = $a . $b;
		if ($a > "") $a = $a . " here.  ";
		// recurse, if needed
		foreach ($tmp as $i) {
			$f = $_allobjs[$i];
			$tmp2 = array();
			if($f->contains) {
			foreach ($f->contains as $k => $i) {
				if ($_allobjs[$i]->isListed()) {
					$tmp2[$k] = $i;
				}
			}
			}
			if (count($tmp2) > 0) {
				$a = $a . $f->listContents();
			}
			//if (count($f->contains) > 0) {
			//	$a = $a . $f->listContents();
			//}
		}
		return $a;
	}
	function __construct() {
		parent::__construct();

		array_push($this->doVerbs, "go");
		$this->cango = array();
		$this->hasseen = FALSE;
		$this->linkPref = "&raquo;&nbsp;";
	}
	function lightIsPresent() { return TRUE; } // for consistency
	function doVerbVerify($verb) {
		global $_allobjs;
		if ($verb == "go") {
			$f = $_allobjs[$_allobjs['_Me']->location];

			if (array_key_exists($this->oid, $f->cango)) {
				return TRUE;
			} else {
				return FALSE;
			}
		}

		return parent::doVerbVerify($verb);
	}

	function enterRoom(&$origoid) {
		// overload this function to perform actions when the player enters the room
		// return any output and the system will pause to display it

	}

	function leaveRoom(&$destoid) {
		// overload this function to perform actions when the player leaves the room
		// return any output and the system will pause to display it

	}

	function doVerbHandle($verb) {
		$result = array();
		global $_allobjs;
		switch ($verb) {
			case "look":
				// We override look because we don't want to list "go" or other verbs
				// and we want firstseen support

				if ($this->hasseen == FALSE) {
					$a = $this->Firstseen();
					if (!is_null($a)) {
						print "<div class='descpane'>" . dynamicString($a);
						print "</div>";
					}
					$this->hasseen = TRUE;
				}
				//print "<div class='descpane'>" . $this->Desc();
				//print "</div>";
				$presult = parent::doVerbHandle($verb);
				$result = array_merge($result, $presult);

				//print $this->showVerbs($verb);
				break;
			case "go":


				// go will be executed in the target class
				// must find _Me and see if the source class can get here
				$f =& $_allobjs['_Me'];

				$f2 =& $_allobjs[$f->location];
				$f3 = $this->oid;
				$pause = FALSE;
				// First, check for leaveroom results
				$a = $f2->leaveRoom($f3);

				if (!is_null($a)) {
					print "<div class='descpane'>" .dynamicString($a) . "</div>";
					$_allobjs['_pause'] = TRUE;
				}

				if (!is_null($f3)) {
					$f4 = $f2->oid; // make a copy
					$a = $_allobjs[$f3]->enterRoom($f4); // otherwise it will be destroyed!
					if (!is_null($f4)) {

						$f->moveInto($_allobjs[$f3]);
					}
					// cannot enter room
					if (!is_null($a)) { 
						//print "<div class='descpane'>" .dynamicString($a) . "</div>";
						$result['message'] = dynamicString($a);
						$_allobjs['_pause'] = TRUE;
					}
				}



				// we need to update the thingy, so reload the page

				/* if (strlen($_allobjs['_interlude']) > 0) {
				 print dynamicString($_allobjs['_interlude']);
				 $_allobjs['_interlude'] = "";
				 $pause = TRUE;
				 } */


				$_allobjs['_refresh'] = TRUE;



				//print "<div class='descpane'>" . $this->Desc();
				//print "</div>";


				break;
			default:
				parent::doVerbHandle($verb);
		}
		return $result;
	}

	function verbIng($verb) {

		switch ($verb) {
			case "go":
				return "going to " . $this->theName();
			default:
				return parent::verbIng($verb);
		}
	}
	
	function defaultVerb() {
		return "go";
	}

}