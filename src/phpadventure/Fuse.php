<?php
/***
 PHPAdventure!   Copyright (C) 2003 Steven Kollmansberger

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 ***/

namespace phpadventure;

/********
 Fuse:  Fuses provide a method for a function to be called on a regular basis.

 The Fuse class must be overloaded to provide the functionality.  The fuse counter ($fuseleft)
 will be decremented every turn until it reachs 0, at which point fuseProcess() will be called.
 If the fuse should execute again, fuseProcess must set fuseleft to the appropriate value.

 New/Updated Properties:
 $fuseleft:  Indicates how many turns until the fuse goes off.  Set to 0 to disable the fuse.

 New/Updated Overloadable Methods:
 fuseProcess(&$verb, &$itemoid, &$dobjoid):  This function is called when the fuse has burned down.  Note that
 it is called before anything is displayed so it should not perform any output.
 If you need output, return a string.  The system will display that output at
 the appropriate time.

 OBSOLETE: If you need to end, win or quit the game, call SendHeader() first, then
 the appropriate game function (don't do this!)

 ********/
class Fuse extends Object {
	var $fuseleft;
	var $docall;
	function __construct() {
		$this->fuseleft = 0; // disabled
		parent::__construct();
	}
	function fuseProcess($verb, $itemoid, $dobjoid) {
		// do something interesting
	}
}

/**** end ad_base.php ****/
