<?php
/***
 PHPAdventure!   Copyright (C) 2003 Steven Kollmansberger

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 ***/

namespace phpadventure;

//use phpadventure\Object;

/********
 Game:  Contains basic game information

 New/Updated Properties:
 $name:  This comes from object, but is used here to mean the title of the game.
 $desc:  Also from object, but consists of a message for the About window
 $footermsg:  Message for the footer (e.g. copyright message) for every page
 $version: Indicates the version number of the game
 $sysverbs: Indicates system verbs handled by this object (add in constructor).
 $cssfile: Indicates the URL to a CSS file to apply to every page
 $title: Indicates what to show in the browser's title bar.  If NULL, the game name will be shown.
 $quitmsg: Displayed when the game is quit.
 $diemsg:  Displayed when the player dies.
 $winmsg: Displayed when the player wins.
 $initmsg: Displayed when the game is first started.
 $turns: Number of turns elapsed
 $score: Current score
 $maxscore: Maximum possible score.  Set to non-zero to enable scoring.


 New/Updated Overloadable Methods:
 handleSysVerb($verb): This handles the request of a system verb.  System verbs appear in the right side panel and may be called
 at any time.
 quitMsg(): This displays the results of quitting.  Can overload if you want to have an interactive quit screen.
 returns TRUE to quit or FALSE to cancel the quit.
 turnStart(&$verb, &$itemoid, &$dobjoid):  This function is called at the beginning of every request that isn't a system verb or a null verb.
 Note that some of the parameters may be unset.  You may make changes to the parameters, but they will
 still have to pass normal checks.
 NOTICE:  This function is called early on and should NOT directly output anything!  Instead, return any string and the system will
 output it after the header is sent.
 NOTICE:  This function handles fuses, so if you overload it, be sure to call parent::turnStart
 otherwise fuses will not work.
 death(): This function is called when the player dies.  it is basically the same as quitting.
 dieMsg(): This displays the results of dying. returns TRUE to quit or FALSE to cancel the death.
 endGame():  Ends the game without displaying any message.
 win():  Ends the game in a win condition
 init():  Called at the beginning of a game.  Returns text to be displayed.

 New/Updated Verbs:
 Quit:  Ends the game
 About:  Displays an about message
 Restart:  Restarts the game


 ********/
class Game extends Object {
	// extends Object only to be stored

	var $version;
	var $sysverbs;
	var $cssfile;
	var $title;
	var $quitmsg;
	var $diemsg;
	var $winmsg;
	var $footermsg;
	var $initmsg;

	var $turns;
	var $score;
	var $maxscore;

	function init() {
		return $this->initmsg;
	}

	function __construct() {
		parent::__construct();
		$this->sysverbs = array("About", "Quit", "Restart", "Restore", "Save", "Wait"); // save handled in ad_main
		$this->quitmsg = "Your session has been terminated.  Thank you for playing.";
		$this->diemsg = "You have died!";
		$this->winmsg = "Congratulations, you have won the game!";
		$this->initmsg = "It was a dark and stormy night...";
		$this->turns = 0;
		$this->score = 0;
		$this->maxscore = 0;
	}

	function quitMsg() {
		print "<table align=center width=40%><tr><td><div align=center class='continue'>" . dynamicString($this->quitmsg) . "</div></td></tr></table>";
		print "</td></tr></table>";
		return TRUE;
	}

	function dieMsg() {
		print "<table align=center width=40%><tr><td><div align=center class='continue'>" . dynamicString($this->diemsg) . "</td></tr></table>";
		print "</td></tr></table>";
		return TRUE;
	}

	function winMsg() {
		print "<table align=center width=40%><tr><td><div align=center class='continue'>" . dynamicString($this->winmsg) . "</td></tr></table>";
		print "</td></tr></table>";
		return TRUE;
	}

	function death() {
		if ($this->dieMsg() == false) return;
		$this->endGame();
	}

	function win() {
		if ($this->winMsg() == false) return;
		$this->endGame();
	}

	function endGame() {
		$_SESSION = array();
		session_destroy();
		print $this->showVerbs(array("Quit", "Save", "Wait"));
		//SendFooter();
		exit();
	}

	function turnEnd($verb, $itemoid, $dobjoid) {
		global $_allobjs;
		$r = "";



		// Handle fuses
		foreach ($_allobjs as $k => $i2) {
			if (strncmp($k, "_", 1) == 0) continue;
			$i =& $_allobjs[$i2->oid];

			if (is_a($i, "Fuse")) {

				if ($i->fuseleft > 0) {
					$i->fuseleft -= 1;

					if ($i->fuseleft == 0) {

						$r2 = $i->fuseProcess($verb, $itemoid, $dobjoid);

						if (!is_null($r2) && strlen($r2) > 0) {
							$r = $r . "<div class='descpane'>" . $r2 . "</div>";
						}

					}
				}
			}
		}
		// handle npcs
		foreach ($_allobjs as $k => $i2) {
			if (strncmp($k, "_", 1) == 0) continue;
			$i =& $_allobjs[$i2->oid];
			if (is_a($i, "NPC")) {
				//if ($i->withMe()) {

				$r2 = $i->NPCDaemon($verb, $itemoid, $dobjoid, $i->withMe());
					
				if (!is_null($r2) && strlen($r2) > 0) {
					$r = $r . "<div class='descpane'>" . $r2 . "</div>";
				}
				//}
			}

		}
		return $r;
	}

	function turnStart(&$verb, &$itemoid, &$dobjoid) {
		if ($verb != "") $this->turns += 1;
	}

	function handleSysVerb($verb) {
		global $_allobjs;
		switch ($verb) {
			case 'init':
				$f = $this->init();
				print "<div class='descpane'>" . dynamicString($f) . "</div>";
				break;
			case 'Quit':
				if ($this->quitMsg() == false) return;
				$this->endGame();
			case 'Restore':
				print "<div class='descpane'>";
				print "<form enctype='multipart/form-data' action='". BaseURL() . "_sysverb=truerestore' method='POST'>";
				// print "<input type='hidden" name="MAX_FILE_SIZE" value="30000">
				print "Select the file to restore from: <input name='savefile' type='file'> ";
				print "<input type='submit' value='Restore'>";
				print "</form></div>";
				break;
			case 'About':
				// Display game and system about info
				print "<div class='descpane'><h3>You are playing " . dynamicString($this->Name()) . "</h3>";
				print dynamicString($this->Desc());
				print "</div><div class='descpane'><h3>This game uses PHPAdventure! version 1.1</h3>";
				print "<a href='http://phpadventure.sf.net'>PHPAdventure!</a> is copyright (c) 2003 <a href='mailto:steven777400@users.sourceforge.net'>Steven Kollmansberger</a> and is released under the <a href='http://www.gnu.org/licenses/gpl.html'>GNU General Public License</a>.  ";
				print "<br><br>PHPAdventure! is designed to bridge the gap between interactive fiction and the web while leaving behind the numerous problems associated with a parser.  Adventure games should be easy to write and fun to play, and that's what PHPAdventure! hopes to do.";
				print "<br><br>Many aspects of PHPAdventure! were based on TADS, the text adventure development system.";
				print "</div>";
				break;
			case 'Restart':
				//$_SESSION = array();
				//session_destroy();
				print "<head><meta http-equiv='refresh' content='0; URL=" . $_SERVER['PHP_SELF'] . "'></head>";
				print "<div class='continue'><a href='" . $_SERVER['PHP_SELF'] . "'>Redirect</a></div></td></tr></table>";
				exit();
		}
	}

	function showVerbs($except = NULL) {

		//$allverbs = array_merge($this->doVerbs, $this->ioVerbs);

		$allverbs = array();
		foreach ($this->sysverbs as $i) {
			if (in_array($i, $except) == FALSE) array_push($allverbs, $i);
		}

		asort($allverbs);
		if (count($allverbs) == 0) return "";
		$a = "<div class='verbpane'><table align=center cellpadding=5><tr>";
		$icnt = 0;
		foreach ($allverbs as $i) {
			$icnt = $icnt + 1;
			$a = $a . "<td ";
			//if ($icnt % 2 == 0) $a = $a . "align=right";
			//if ($icnt % 2 == 1 && $icnt < count($allverbs)) $a = $a . "align=left";
			$a = $a . ">";
			$a = $a . "<a href='" . BaseURL() . "_sysverb=" . $i . "'>" . $i . "</a></td>";
			//if ($icnt % 2 == 0) $a = $a . "</tr><tr>";


		}
		$a = $a . "</tr></table></div>";

		return $a;
	}

}
