<?php
/***
 PHPAdventure!   Copyright (C) 2003 Steven Kollmansberger

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 ***/

namespace phpadventure;

/***********
 Container:  An object which can contain other objects

 New/Updated Properties:
 $isQuiet:  If set to true, items inside the container will not be listed except when looking
 directly at the container.
 $putinMsg:  Shown when something is put in the container
 $canContain:  Since many things derive from container, this is allowed to turn off the
 container functionality.  Set to FALSE to disable the putin verb and listcontents.

 Useful overloadable methods:
 contentsChanged($entered, &$obj):  Called when an object is added or removed from the contents
 entered will be TRUE if the object is added or FALSE otherwise.  obj will be the object in question.

 New/Updated Verbs:
 putin: puts another Carryable (that is not fixed) into this container
 Objects can be removed from a container by putting them in another container
 or executing take on them




 ******/

class Container extends Carryable {
	var $isQuiet;
	var $putinMsg;
	var $canContain;

	function Desc() {
		$a = parent::Desc();
		$a = $a . "<br>" . $this->listContents(TRUE);
		return $a;
	}
	function listContents($lookdir = FALSE) {
		if (($lookdir == FALSE && $this->isQuiet) || !$this->canContain) return "";
		global $_allobjs;
		$tmp = array();
		foreach ($this->contains as $k => $i) {
			if ($_allobjs[$i]->isListed()) {
				$tmp[$k] = $i;
			}
		}

		if (count($tmp) == 0) {
			return ucfirst($this->theName()) . " is empty. ";
		}
		$a = "Inside " . $this->theName() . " you see ";
		$b = "";
		foreach ($tmp as $i) {
			if ($c > "") $b = ", " . $b;
			$c = $c . $b;
			$f = $_allobjs[$i];

			$b = $f->objRef("look", $f->aName());


		}
		$a = $a . $c;
		if ($c > "") $a = $a . " and " ;
		$a = $a . $b;
		if ($a > "") $a = $a . ".  ";
		// recurse, if needed
		foreach ($tmp as $i) {
			$f = $_allobjs[$i];
			$tmp2 = array();
			foreach ($f->contains as $k => $i) {
				if ($_allobjs[$i]->isListed()) {
					$tmp2[$k] = $i;
				}
			}
			if (count($tmp2) > 0) {
				$a = $a . $f->listContents();
			}
		}
		return $a ;
	}

	function __construct() {


		parent::__construct();
		array_push($this->ioVerbs, "putin");
		$this->isQuiet = FALSE;
		$this->putinMsg = "Done.";
		$this->canContain = TRUE;

	}

	function ioVerbVerify($verb, $dobj) {
		if ($verb == "putin") {
			// we could check dobj to allow only certain types of objects
			// to be put in the container

			if (array_key_exists($dobj->oid, $this->contains) || !$this->canContain) return FALSE; // only at top level, lower is ok!

			return TRUE;
		}
		return parent::ioVerbVerify($verb, $dobj);
	}

	function ioVerbHandle($verb, &$dobj) {
		global $_allobjs;
		switch ($verb) {
			case "putin":
				$dobj->moveInto($this);
				print "<div class='descpane'>" . dynamicString($this->putinMsg) . "</div>";
				break;
			default:
				parent::ioVerbHandle($verb, $dobj);
		}
	}


	function verbIo($verb, $dobj) {
		global $_allobjs;
		switch ($verb) {
			case "putin":
				return "Put " . $dobj->theName() . " in " . $this->theName();
			default:
				return parent::verbIo($verb, $dobj);
		}
	}

	function verbIoIng($verb, $dobj) {
		switch ($verb) {
			case "putin":
				return "Putting " . $dobj->theName() . " in " . $this->theName();
			default:
				return parent::verbIoIng($verb, $dobj);
		}
	}


}