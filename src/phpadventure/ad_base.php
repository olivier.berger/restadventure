<?php

//namespace phpadventure\Base;

/****  ad_base.php:  This file contains the foundational functions and objects to execute a game ****/
/***
	PHPAdventure!   Copyright (C) 2003 Steven Kollmansberger

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
***/

// // for PHP < 4.2.0
// if (!function_exists('is_a')) {
//   function is_a( $object, $className ) {
//     return ((strtolower($className) == get_class($object))
//           or (is_subclass_of($object, $className)));
//   }
// }

// if (!function_exists('file_get_contents')) {
// 	function file_get_contents($filename) {
//     	$fd = fopen("$filename", "rb");
//     	$content = fread($fd, filesize($filename));
//     	fclose($fd);
//     	return $content;
// 	}
// }

function BaseURL() {
	return $_SERVER['PHP_SELF'] . "?";
}



function filterHtml($instr) {
	$ishtml = FALSE;
	$a = "";
	for ($i = 0; $i < strlen($instr); $i++) {
		$b = substr($instr, $i, 1);
		if ($b == "<") $ishtml = TRUE;

		if (!$ishtml) $a = $a . $b;
		if ($b == ">") $ishtml = FALSE;
	}
	return $a;
}

function dynamicString($strng) {
		// this function should find a << code >> sections and execute them
		// re-creating the needed dynamic string
		// << code >> sections should not change any state nor do any output
		// they should return what they want to appear
		// << code >> should be put in single quotes to avoid variable evaluation
		
		$newstrng = "";
		$func = "";
		$isfunc = FALSE;

		for ($i = 0; $i < strlen($strng); $i++) {
			
			if (substr($strng, $i, 2) == "<<" && $isfunc == FALSE) {
				$i++; // skip the second <

				$isfunc = TRUE;
				$func = "";
			} elseif (substr($strng, $i, 2) == ">>" && $isfunc == TRUE) {
				$isfunc = FALSE;
				$i++; // skip the second >

				// execute function and append to newstring
				$newfunc = create_function('', 'global $_allobjs; ' . $func);
				$newstrng .= $newfunc();

				//$newstrng .= eval($func);
			} elseif ($isfunc == FALSE) {
				$newstrng .= substr($strng, $i, 1);
			} elseif ($isfunc == TRUE) {
				$func .= substr($strng, $i, 1);
			}
		}
		return $newstrng;
	}


?>
