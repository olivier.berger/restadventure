<?php

/***
 PHPAdventure!   Copyright (C) 2003 Steven Kollmansberger

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 ***/

namespace phpadventure;

/**************

Surface:  A container that things go "on" instead of "in".  Otherwise exactly identical to a container

**************/

class Surface extends Container {
	function listContents($lookdir = FALSE) {
		if (($lookdir == FALSE && $this->isQuiet) || !$this->canContain) return "";
		global $_allobjs;
		$tmp = array();
		foreach ($this->contains as $k => $i) {
			if ($_allobjs[$i]->isListed()) {
				$tmp[$k] = $i;
			}
		}

		if (count($tmp) == 0) {
			return "There's nothing on " . $this->theName() . ". ";
		}
		$a = "On " . $this->theName() . " you see ";
		$b = "";
		$c = '';
		foreach ($tmp as $i) {
			if ($c > "") $b = ", " . $b;
			$c = $c . $b;
			$f = $_allobjs[$i];

			$b = $f->objRef("look", $f->aName());
			$b = $f->htmlLinkObjRef($b);

		}
		$a = $a . $c;
		if ($c > "") $a = $a . " and " ;
		$a = $a . $b;
		if ($a > "") $a = $a . ".  ";
		// recurse, if needed
		foreach ($tmp as $i) {
			$f = $_allobjs[$i];
			$tmp2 = array();
			foreach ($f->contains as $k => $i) {
				if ($_allobjs[$i]->isListed()) {
					$tmp2[$k] = $i;
				}
			}
			if (count($tmp2) > 0) {
				$a = $a . $f->listContents();
			}
		}
		return $a ;
	}
	function verbIo($verb, $dobj) {
		global $_allobjs;
		switch ($verb) {
			case "putin":
				return "Put " . $dobj->theName() . " on " . $this->theName();
			default:
				return parent::verbIo($verb, $dobj);
		}
	}
	function verbIoIng($verb, $dobj) {
		switch ($verb) {
			case "putin":
				return "Putting " . $dobj->theName() . " on " . $this->theName();
			default:
				return parent::verbIoIng($verb, $dobj);
		}
	}
}
