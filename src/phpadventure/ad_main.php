<?php
/**** ad_main.php:  This file contains the main execution system and is the starting point for the game. ****/

/***
	PHPAdventure!   Copyright (C) 2003 Steven Kollmansberger

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
***/

// before we even include the files, check the version of the interpreter.

// function version_check($version)
// {
//    $testSplit = explode ('.', $version);
//     $currentSplit = explode ('.', phpversion ());

//     if ($testSplit[0] < $currentSplit[0])
//         return True;
//     if ($testSplit[0] == $currentSplit[0]) {
//         if ($testSplit[1] < $currentSplit[1])
//             return True;
//         if ($testSplit[1] == $currentSplit[1]) {
//             if ($testSplit[2] <= $currentSplit[2])
//                 return True;
//         }
//     }
//     return False;
// }

// if(version_check('4.1.0') == FALSE) die("PHP version 4.1.0 or greater is required.");


include_once("encrypt.php");
include_once("ad_base.php");
include_once("ad_classes.php");
include_once("ad_npc.php");
// **** Add any third-party supplied class extensions here! e.g. include_once("ad_my_cool_classes.php");


$_stage = 1;
include($_mygamefile);


session_start();
global $_allobjs;
$_allobjs = array();

if ($_REQUEST['_sysverb'] == "truerestore") {
	// must have a file
	
	if (is_null($_FILES['savefile']['tmp_name']) || $_FILES['savefile']['tmp_name'] == "") {
		print "<p align=center>Restore Error:  File upload failed!</p>";
		exit();
	}

	$_f = new encrypt($_mygamecode);
	$_f = $_f->cipher_string(unserialize(file_get_contents($_FILES['savefile']['tmp_name'])));

	$_SESSION['vars'] = $_f;
	$_f = unserialize($_f);
	if (!isset($_f['_Game'])) {
		// error!
		print "<p align=center>Restore Error:  File corrupt or for a different game!</p>";
		exit();
	}
	unset($_f);

}

if (!isset($_SESSION['vars'])) {


	$_Game = new Game(); // create a default game object
	$_Game->oid = "_Game";
	$_allobjs['_Game'] =& $_Game;

	$_Me = new Object(); // create a default Me object
	$_Me->oid = "_Me";
	$_allobjs['_Me'] =& $_Me;

	$_Startroom = new Room(); // create a default starting room
	$_Startroom->oid = "_Startroom";
	$_allobjs['_Startroom'] =& $_Startroom;

	$_Me->moveInto($_Startroom);



	// Note: any of the above objects can be recreated from scratch by simply remaking them
	// and updated thier allobjs reference.
	$_stage = 2;
	include($_mygamefile);

	?><?php
	
	$_sysverb = $_REQUEST['_sysverb'];
	if (!isset($_sysverb)) $_sysverb = "init";

} else {
	/* RESTORE SESSION - must not be in a function */
	$_f = unserialize($_SESSION['vars']);
	foreach ($_f as $k => $i) {
		if (isset($$k)) continue;

		$$k = $i;
		if (is_a($i, "Object")) {

			$_allobjs[$k] =& $$k;
		}

	}


	$_sysverb = $_REQUEST['_sysverb'];
	$_verb = $_REQUEST['_verb'];
	$_item = $_REQUEST['_item'];
	$_dobj = $_REQUEST['_dobj'];
	$_misc = $_REQUEST['_misc'];

}


unset($_f);

if ($_sysverb == "Save") {
		unset($_f);

		unset($_stage);
		unset($_verb);
		unset($_sysverb);
		unset($_item);
		unset($_dobj);
		unset($_allobjs);
		unset($_misc);
		//unset($_mygamefile); keep this
		$_f = get_defined_vars();
		unset($_f['_SESSION']);
		unset($_f['HTTP_SESSION_VARS']);
		unset($_f['vars']);
		$_SESSION['vars'] = serialize($_f);
		$_f = new encrypt($_mygamecode);
		$_f = $_f->cipher_string($_SESSION['vars']);
		header("Content-type: application/phpadvsave");
		header("Content-Disposition: attachment; filename=" . session_name() . ".sav");
		print serialize($_f);
		exit();
}
if ($_sysverb == "Wait") {
	unset($_sysverb);
	$_verb = "Wait";
}

if (is_null($_sysverb)) {
	$_allobjs['_interlude'] = $_Game->turnStart($_verb, $_item, $_dobj);
}

$_allobjs['_misc'] = $_misc;
$_allobjs['_sysverb'] = $_sysverb;
$_allobjs['_verb'] = $_verb;
$_allobjs['_dobj'] = $$_dobj;
$_allobjs['_item'] = $$_item;

SendHeader();

// output f if need  from game start
/* if (strlen($_f) > 0) {
	print $_f;
	$_allobjs['_pause'] = TRUE;
} */

$_f = $_Me->location;


if (!is_null($_sysverb)) {



	$_Game->handleSysVerb($_sysverb);

	print "<table align=center width=40%><tr><td><div align=center class='continue'><a href='" . $_SERVER['PHP_SELF'] . "'>Continue</a></div>";
	print "</td></tr></table>";



} elseif (!is_null($_dobj)) {
	// it's a complete io command
	// check 1: are items reachable?
	if ($$_f->isReachable($$_item) == FALSE && $_Me->isReachable($$_item) == FALSE) {
		trigger_error("Out of reach item on object " . $$_item->name . " (" . $_item . ")");

		exit();
	}
	if ($$_f->isReachable($$_dobj) == FALSE && $_Me->isReachable($$_dobj) == FALSE) {
		trigger_error("Out of reach item on object " . $$_dobj->name . " (" . $_dobj . ")");

		exit();
	}

	// check 2: is verb verified?

	if ($$_dobj->doVerbVerify($_verb) == FALSE) {
		trigger_error("Unverified doVerb " . $_verb . " on object " . $$_dobj->name . " (" . $_dobj . ")");

		exit();
	}
	if ($$_item->ioVerbVerify($_verb, $$_dobj) == FALSE) {
		trigger_error("Unverified ioVerb " . $_verb . " on object " . $$_item->name . " (" . $_item . ")");

		exit();
	}
	
	$$_item->ioVerbHandle($_verb, $$_dobj);
	$_f = dynamicString($_allobjs['_interlude']);

	$_f .= dynamicString($_Game->turnEnd($_verb, $_item, $_dobj));
	print $_f;

	if ($_allobjs['_refresh'] && !$_allobjs['_pause'] && strlen($_f) == 0) {
		print "<head><meta http-equiv='refresh' content='0; URL=" . $_SERVER['PHP_SELF'] . "'></head>";
		print "<div class='continue'><a href='" . $_SERVER['PHP_SELF'] . "'>Redirect</a></div>";
	}
	print "<table align=center width=40%><tr><td><div align=center class='continue'><a href='" . $_SERVER['PHP_SELF'] . "'>Continue</a></div>";
	print "</td></tr></table>";




} elseif (!is_null($_verb) && $_verb != "Wait") {
	// process a command

	// check 1: is item reachable?
	if ( ($$_f->isReachable($$_item) == FALSE && $_Me->isReachable($$_item) == FALSE) && $_verb != "go") { // go verbs do not have to be reachable.  the dest-oking is handed by verify
		trigger_error("Out of reach item on object " . $$_item->name . " (" . $_item . ")");

		exit();
	}
	// check 2: is verb verified?
	if ($$_item->doVerbVerify($_verb) == FALSE) {
		trigger_error("Unverified verb " . $_verb . " on object " . $$_item->name . " (" . $_item . ")");

		exit();
	}
	// execute verb

	$$_item->doVerbHandle($_verb);
	$_f = dynamicString($_allobjs['_interlude']);

	$_f .= dynamicString($_Game->turnEnd($_verb, $_item, $_dobj));
	print $_f;

	if ($_allobjs['_refresh'] && !$_allobjs['_pause'] && strlen($_f) == 0) {		
		print "<head><meta http-equiv='refresh' content='0; URL=" . $_SERVER['PHP_SELF'] . "'></head>";
		print "<div class='continue'><a href='" . $_SERVER['PHP_SELF'] . "'>Redirect</a></div>";
	}
	print "<table align=center width=30%><tr><td><div align=center class='continue'><a href='" . $_SERVER['PHP_SELF'] . "'>Continue</a></div>";
	print "</td></tr></table>";

} else {
	// no explicit verb.  Find my location and show it
	unset($_verb); // in case of Wait
	
	$$_f->doVerbHandle("look");
	print dynamicString($_allobjs['_interlude']);

	print dynamicString($_Game->turnEnd($_verb, $_item, $_dobj));
}

/* if (is_null($_sysverb)) {
	$_f = $_Game->turnEnd($_verb, $_item, $_dobj);
	print dynamicString($_f);

} */


FinishPageFooter();
SendFooter();

/* SAVE SESSION - must not be in a function */
unset($_f);

unset($_misc);
unset($_stage);
unset($_verb);
unset($_sysverb);
unset($_item);
unset($_dobj);
unset($_allobjs);
unset($_mygamefile);
$_f = get_defined_vars();
unset($_f['_SESSION']);
unset($_f['HTTP_SESSION_VARS']);
unset($_f['vars']);
$_SESSION['vars'] = serialize($_f);


?>

