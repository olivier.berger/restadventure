<?php
/***
	PHPAdventure!   Copyright (C) 2003 Steven Kollmansberger

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
***/

namespace phpadventure;

/*************
	NPC: Non-Player Charectors
	
	New/Updated Properties:
		$itSelf:  How should the NPC be referred to?  Usually "itself", "himself" or "herself"
		$dontKnow:  What should the NPC say when questioned about something not in the ask-about database?
		$randomMessages: An array of messages one of which will be displayed at random at every turn.  Use empty messages to display nothing.

	Overloadable Methods:
		NPCDaemon($verb, $itemoid, $dobjoid, $mehere):  Called every step, indicates what the calling state is.  $mehere indicates if the NPC is withMe().
		
	Useful Methods:
		askAddObject($obj, $response): Adds a response for a given object.
		askAddTopic($topicname, $response): Adds a response for a given topic

		convAddLink($srcnode, $dstnode, $statement): Adds a link between two pre-existing conversation nodes
		convAddNode($node, $statement): Adds a conversation node.
		
		Note:  Conversation trees and ask-abouts are mutually exclusive - you cannot use both methods.
		Select the method you want by adding to it.  If you don't add anything to either method, talk to
		verb will not be enabled.  The default (beginning) conversation node must be called _Startnode
**********/

/** todo:  ability to tell npc about something, ability to give something to npc,
	npc track (random?) movement
***/

class NPC extends Carryable {
	var $convlinks;
	var $convdesc;
	var $askobjs;
	var $asktopics;
	var $itSelf;
	var $currconvnode;
	var $dontKnow;
	var $randomMessages;

	function aName() { return $this->Name(); }
	function theName() { return $this->Name(); }


	function NPCDaemon($verb, $itemoid, $dobjoid, $mehere) {
		// show a random message, if available
		// since this is a pre-turn action, no direct output is permitted.

		if (count($this->randomMessages) > 0 && $verb != "go" && $mehere) {
			return $this->randomMessages[array_rand($this->randomMessages)];
		}
	}
	function __construct() {
		parent::__construct();
		$this->isFixed = TRUE;
		$this->isListed = TRUE;
		$this->convlinks = array();
		$this->convdesc = array();
		$this->askobjs = array();
		$this->asktopics = array();
		$this->itSelf = "itself";
		$this->currconvnode = NULL;
		$this->dontKnow = "\"I don't know much about that.\""; // '
		$this->randomMessages = array();

		array_push($this->doVerbs, "talkto");
	}

	function doVerbVerify($verb) {
		global $_allobjs;
		if ($verb == "talkto") {
			if (count($this->convdesc) > 0) return TRUE;
			if (count($this->askobjs) == 0 && count($this->asktopics) == 0) return FALSE;
			foreach ($_allobjs as $i) {
				if (is_a($i, "phpadventure\Object")) {
					$k = $i->oid;
					if ($this->askObjVerify($i)) { return TRUE; }
				}
			}

			foreach ($this->asktopics as $k => $i) {
				if ($this->askTopicVerify($k)) return TRUE;
			}


			return FALSE;
		} else return parent::doVerbVerify($verb);
	}

	function verbIng($verb) {
		if ($verb == "talkto") return "talking to " . $this->Name(); else return parent::verbIng($verb);
	}
	function verbDo($verb) {
		if ($verb == "talkto") return "Talk to " . $this->Name(); else return parent::verbDo($verb);
	}
	function doVerbHandle($verb) {
		global $_allobjs;
		if ($verb == "talkto") {
			$misc = $_allobjs['_misc'];
			if (is_null($misc)) {
				// for askabout, send the page
				// for conv, start the conv
				if (count($this->convdesc) > 0) {
					$this->currconvnode = "_Startnode";
					return $this->convNodeDisplay();
				}
				print dynamicString($_allobjs['_interlude']);
				$_allobjs['_interlude'] = "";
				return $this->askDisplay();
			}
			if (!is_null($this->currconvnode)) {
				if ($this->convLinkVerify($misc)) {
					return $this->convLinkHandle($misc);
				} else {
					trigger_error("Unverified link conv link " . $misc);
					return;
				}
			}
			if (!is_null($_allobjs[$misc])) {
				if ($this->askObjVerify($_allobjs[$misc])) {
					return $this->askObjHandle($_allobjs[$misc]);
				}
			}

			if ($this->askTopicVerify($misc)) {
				return $this->askTopicHandle($misc);
			}
			trigger_error("Unknown talk action " . $misc);

		} else return parent::doVerbHandle($verb);
	}



	/** Ask About:  User asks about an object that they have with them
		or about a pre-defined topic **/

	function askAddObject($obj, $response = NULL) {
		$this->askobjs[$obj->oid] = $response;
	}
	
	function askAddTopic($topicname, $response = NULL) {
		$this->asktopics[$topicname] = $response;
	}
	function askObjVerify($obj) {

		if ($obj->withMe()) return TRUE; //array_key_exists($obj->oid, $this->askobjs);
		return FALSE;
	}
	function askTopicVerify($topicname) {
		return array_key_exists($topicname, $this->asktopics);
	}
	function askObjHandle($obj) {
		$result = array();
		if (array_key_exists($obj->oid, $this->askobjs)) {
			$response = dynamicString($this->askobjs[$obj->oid]);
		} else {
			$response = dynamicString($this->dontKnow);
		}
		//print "<div class='descpane'>" . $response . "</div>";
		$result['message'] = $response;
		return $result;
	}
	function askTopicHandle($topicname) {
		print "<div class='descpane'>" . dynamicString($this->asktopics[$topicname]) . "</div>";
	}
	function askDisplay() {
		$result = array();
		global $_allobjs;

		$mylinks = array();
		foreach ($_allobjs as $i) {
			if (is_a($i, "phpadventure\Object")) {
				$k = $i->oid;
				if ($this->askObjVerify($i)) { 
					if ($k == $this->oid)
						$mylinks[$k] = $this->itSelf;
					else
						$mylinks[$k] = $i->theName();
				}
			}
		}
		foreach ($this->asktopics as $k => $i) {
			if ($this->askTopicVerify($k)) { $mylinks[$k] = $k; }
		}
		asort($mylinks);
		if (count($mylinks) == 0) return "";

		$a = "<div class='verbpane'><table align=center cellpadding=5><tr>";
		$icnt = 0;
		foreach ($mylinks as $k => $i) {
			$icnt = $icnt + 1;
			$a = $a . "<td ";
			if ($icnt % 2 == 0) $a = $a . "align=right";
			if ($icnt % 2 == 1 && $icnt < count($mylinks)) $a = $a . "align=left";
			$a = $a . ">";
			$a = $a . $this->htmlLinkObjRef($this->objRef("talkto", "Ask " . $this->Name() . " about " . $i, NULL, $k)) . "</td>";
			if ($icnt % 2 == 0) $a = $a . "</tr><tr>";
		}
		$a = $a . "</tr></table></div>";
		print dynamicString($a);
		return $result;

	}
	
	/** Conversation Tree:  Users is presented with a series of things they can say.  Selecting one,
		they move to a new node in the tree. **/

	function convAddLink($srcnode, $dstnode, $statement) {
		if (array_key_exists($srcnode, $this->convdesc) == FALSE || array_key_exists($dstnode, $this->convdesc) == FALSE) {
			trigger_error("Both nodes in a converstion must exist before a link can be made on ". $srcnode . " -> " . $dstnode);
			return;
		}
		$this->convlinks[$srcnode."->".$dstnode] = $statement;
	}

	function convAddNode($node, $statement) {
		$this->convdesc[$node] = $statement;
	}

	function convLinkVerify($dstnode) {
		return array_key_exists($this->currconvnode . "->" . $dstnode, $this->convlinks);
	}
	
	function convLinkHandle($dstnode) {
		$this->currconvnode = $dstnode;
		return $this->convNodeDisplay();
	}

	function convNodeDisplay() {
		global $_allobjs;
		print "<div class='descpane'>" . dynamicString($this->convdesc[$this->currconvnode]) . "</div>";
		print dynamicString($_allobjs['_interlude']);
		$_allobjs['_interlude'] = "";
		$mylinks = array();
		foreach ($this->convdesc as $k => $i) {
			// verify and add to array
			if ($this->convLinkVerify($k)) {
				array_push($mylinks, $k);
			}
		}
		asort($mylinks);
		if (count($mylinks) == 0) return "";

		$a = "<div class='verbpane'><table align=center cellpadding=5><tr>";
		$icnt = 0;
		foreach ($mylinks as $i) {
			$icnt = $icnt + 1;
			$a = $a . "<td ";
			if ($icnt % 2 == 0) $a = $a . "align=right";
			if ($icnt % 2 == 1 && $icnt < count($mylinks)) $a = $a . "align=left";
			$a = $a . ">";
			$a = $a . $this->objRef("talkto", $this->convlinks[$this->currconvnode . "->" . $i], NULL, $i) . "</td>";
			if ($icnt % 2 == 0) $a = $a . "</tr><tr>";
		}
		$a = $a . "</tr></table></div>";
		print dynamicString($a);
	}



	function defaultVerb() {
		return "look";
	}


}
/*** end ad_npc.php ***/
?>
