<?php

use Doctrine\Common\Annotations\AnnotationRegistry;
use Composer\Autoload\ClassLoader;

/**
 * @var ClassLoader $loader
 */
$loader = require __DIR__.'/../vendor/autoload.php';

$loader->add('phpadventure', __DIR__.'../src/phpadventure/');

AnnotationRegistry::registerLoader([$loader, 'loadClass']);

return $loader;
