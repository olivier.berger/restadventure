<?php
if (!isset($_stage)) {
	session_name("testgame");
	$_mygamefile = "game_testgame.php";
	include("ad_main.php");
} elseif ($_stage == 1) {


	
} else {
	// Note: All object definitions must be at top-level (i.e. here, not in functions) or they will be lost!

	// Default game, startroom and me objects are already declared.

	$_Game->name = "A PHPAdventure! Game";
	$_Game->version = "1.0";
	$_Game->desc = "This game copyright thus-and-when by so-and-so, thanks to etc, etc, whatever.  This game is based on blah-blah.";

	$gt = new Carryable();
	$gt->name = "goat";
	$gt->desc = "GOAT!";
	$gt->emitsLight = TRUE;
	$gt->oid = "gt";
	$_allobjs['gt'] =& $gt;

	$bx = new Lockable(); // "box", "A big box"
	$bx->addKey($gt);
	$bx->keylessLock = FALSE;
	$bx->keylessUnlock = FALSE;
	$bx->isLocked = TRUE;
	$bx->isTransparent = TRUE;
	$bx->name = "box";
	$bx->desc = "A big box.";
	$bx->oid = "bx";
	$_allobjs['bx'] =& $bx;



	$g2 = new Object();
	$g2->name = "g2";
	$g2->desc = "G2!";
	$g2->oid = "g2";
	$_allobjs['g2'] =& $g2;

	$g3 = new Carryable();
	$g3->name = "g3";
	$g3->desc = "G3!";
	$g3->oid = "g3";
	$_allobjs['g3'] =& $g3;


	$op = new Container();
	$op->name = "jar";
	$op->desc = "A pretty jar.";
	$op->oid = "op";
	$_allobjs['op'] =& $op;


	$_Startroom->name = "Start Room";
	$_Startroom->desc = "A sandy beach.  A " . $g2->fixedObjRef() . " sits in the sand. To the " . $_Startroom->addGo("oroom", "north") . " is a dark cave.";
	$_Startroom->oid = "_Startroom";
	$_allobjs['_Startroom'] =& $_Startroom;



	$oroom = new Darkroom();
	$oroom->firstseen = "Dance revolution!";
	$oroom->name = "Other Room";
	$oroom->desc = "A dark cave.  To the " . $oroom->addGo("_Startroom", "south") . " is a sandy beach.";
	$oroom->oid = "oroom";
	$_allobjs['oroom'] =& $oroom;

	$bx->moveInto($_Startroom);
	$op->moveInto($bx);
	$gt->moveInto($_Startroom);
	$g2->moveInto($_Startroom);

	$g3->moveInto($oroom);
}


/**** end of file ad_stdgame.php ****/
?>
