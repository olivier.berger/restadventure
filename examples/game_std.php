<?php
/**** game_std.php:  This file contains a starting place for the creation of a game.  ****/


// This file is actually called twice.
// Once to get the game name for the session AND
// get custom classes (which are needed on each run) (stage 1)
// The second time to instantiate all the objects.  (only done once per session!) (stage 2)
if (!isset($_stage)) {
	
	session_name("phpadventuregame"); // set an alphanumeric identifier
	$_mygamefile = "game_std.php"; // this filename
	$_mygamecode = "globally unique code"; // put some garbage or other random unique code here
	include("ad_main.php"); // leave this unchanged

} elseif ($_stage == 1) {
	// Declare your custom classes for this game here!

} else {
	// Note: All object definitions must be at top-level (i.e. here, not in functions) or they will be lost!

	// Default game, startroom and me objects are already declared.

	$_Game->name = "A PHPAdventure! Game";
	$_Game->version = "1.0";
	$_Game->desc = "This game copyright thus-and-when by so-and-so, thanks to etc, etc, whatever.  This game is based on blah-blah.";

	$_Startroom->name = "Default Room";
	$_Startroom->desc = "You are in a blank, empty, nondescript room.  There is nothing to do.  Better add some stuff!";

} 
/**** end of file game_std.php ****/
?>
