<?php
if (!isset($_stage)) {
	
	session_name("perryhouse"); // set an alphanumeric identifier
	$_mygamefile = "game_dream.php"; // this filename
	$_mygamecode = "f4h89fhq43foa"; // put some garbage or other random unique code here
	include("ad_main.php"); // leave this unchanged

} elseif ($_stage == 1) {
	// Declare your custom classes for this game here!
	class MikalNPC extends NPC {
		// we want Mikal to follow the player everywhere the player goes
		// until bad stuff happens
		function NPCDaemon($verb, $itemoid, $dobjoid, $mehere) {
			global $_allobjs;
			if ($verb == "go" && !is_null($this->location) && !is_a($_allobjs[$itemoid], "NoEntranceRoom") && $itemoid != "b2_mid") {
				$this->moveInto($_allobjs[$itemoid]);
				return $this->Name() . " follows you cautiously.";
			} else return parent::NPCDaemon($verb, $itemoid, $dobjoid, $mehere);
		}


	}

	class FallRoom extends Darkroom {
		function enterRoom(&$origoid) {
		

			if ($origoid != "_Startroom") return parent::enterRoom($origoid);

			// we want the user to fall down into the pit of despair.
			global $_allobjs;
			$_allobjs['Mikal']->moveOut();
			$_allobjs['MikalFuse']->fuseleft = 1; // engage the fust
			$_allobjs['BleedFuse']->fuseleft = 1; // engage the fust
			$fs= "As you head south, a sudden cracking sound erupts from the floorboards. \"Look out!\" Mikal calls out.
		Before you can respond, the floor gives way beneath you and you fall through.  Timbers and boards flash around you.
		You impact another wooden floor, apparently some kind of basement, and crash straight through it as well.
		With a resounding thud you impact on a concrete floor.  You've fallen nearly thirty feet, landing hard on the concrete floor.
		Your leg has been cut up by the fall pretty bad -- blood pours out onto the floor. ";
			if ($_allobjs['flashlight']->isIn($_allobjs['_Me'])) {
				$_allobjs['flashlight']->moveInto($_allobjs['b2_mid']);
				$fs .= "Your flashlight lays nearby amidst the debry, weakly illuminating this dark cavern.";
			}
			$origoid = NULL; // cancel movement
			$_allobjs['_Me']->moveInto($_allobjs['b2_floor']); // and do our own
			return $fs;

		}

	}

	class BleedFuse extends Fuse {
		var $bleedTime;
		function fuseProcess() {
			global $_allobjs;
			$this->fuseleft = 1;

			if ($_allobjs['bandage']->isWorn) {
			 	if ($this->bleedTime > 0) {
					$this->bleedTime = 0;
					$_allobjs['bandage']->isblood = TRUE;
				} 
				return;
			}

			// make the current room have bllod
			// $_allobjs[$_allobjs['_Me']->location]->isblood = TRUE;
			// check for bandage
			$this->bleedTime += 1;
			switch ($this->bleedTime) {
			case 2:
				return  "You need to get this bleeding taken care of quickly.";
			case 7:
			case 10:
				return "You're beginning to feel light-headed due to the continued loss of blood.";
			case 13:
			case 14:
			case 15:
				return "If you don't get the bleeding stopped immediately, you will pass out and bleed to death.";
			case 16:
				//$_allobjs['_Game']->diemsg = "Having lost copious amounts of blood, you collapse onto the floor, never to be heard from again.";
				//SendHeader();
				print "<div class='descpane'>Having lost copious amounts of blood, you collapse onto the floor, never to be heard from again.</div>";
				$_allobjs['_Game']->death();

			};
			


		}
	}

	class DarkFuse extends Fuse {
		var $darkTime;
		function fuseProcess() {
			global $_allobjs;
			$this->fuseleft = 1;
			$ml = $_allobjs['_Me']->location;
			if (is_a($_allobjs[$ml], "darkroom") && $_allobjs[$ml]->lightIsPresent() == FALSE) {
				$this->darkTime += 1;
				switch ($this->darkTime) {
				case 1:
					return "The darkness feels as if it is swooping in, enveloping you in a thick, oppresive emptiness.  From the darkness, moans and cries echo.";
				case 2:
					return "You can feel hands brushing against you in the darkness, and the moans and cries grow louder.";
				case 3:
					//$_allobjs['_Game']->diemsg = "The darkness overwhelms you, as the hands drag you down, down beyond the floor and into oblivion.";
					//SendHeader();
					print "<div class='descpane'>The darkness overwhelms you, as the hands drag you down, down beyond the floor and into oblivion.</div>";
					$_allobjs['_Game']->death();
				}

			} else $this->darkTime = 0;


		}
	}
	
	class BloodFloor extends SitLayItem { // we could do surface and link it to the room, but why bother?



		function doVerbVerify($verb) {
			if ($verb == "in") return FALSE;
			return parent::doVerbVerify($verb);
		}

	}
	
	class BloodRoom extends Darkroom {
		function Desc() {
			global $_allobjs;
			$a = parent::Desc();
			$a .= ($this->isblood ? "Small pools of fresh blood sit on the concrete floor. " : "");
			return $a;
		}
		function leaveRoom($origoid) { // not perfect, but avoids immediate display
			global $_allobjs;
			if ($_allobjs['BleedFuse']->bleedTime > 0) $this->isblood = TRUE;
			parent::leaveRoom($origoid);
		}
	}

	class VisionObject extends SitLayItem {
		var $triggered;
		var $triggermsg;
		function VisionObject() {
			SitLayItem::SitLayItem();
			$this->canSitLay = FALSE;
			$this->canContain = FALSE;
			$this->triggered = FALSE;
			$this->isFixed = FALSE;

		}
		function doVerbHandle($verb) {
			if ($verb != "look" && !$this->triggered) {
				print "<div class='descpane'>" . dynamicString($this->triggermsg) . "</div>";
				$this->triggered = TRUE;
			} else return parent::doVerbHandle($verb);
		}
	}


} else {
	// Note: All object definitions must be at top-level (i.e. here, not in functions) or they will be lost!

	// Default game, startroom and me objects are already declared.

	$_Game->name = "The Perry House";
	$_Game->version = "1.0";
	$_Game->maxscore = 3;
	$_Game->desc = "The Perry House is copyright (c) 2003 Steven Kollmansberger.  I got the initial idea 
	for a ghost/horror adventure from watching the 1999 remake of 'House on Haunted Hill'.  
	To create the plot, I choose elements from some urban infiltration accounts I have read,
	combined with things I've seen in other Interactive Fiction games along with a little
	good old-fashioned creativity  Hope you like it!";
	$_Game->footermsg = "The Perry House is copyright (c) 2003 Steven Kollmansberger";
	$_Game->initmsg = "In 1913, Dr. Rudolph Perry established the Perry House for the Insane.  Through the years,
    it was shroaded in mystery and secrecy, with rumors of medical experiments and horrible
    abuse abounding.  No one, it was said, who was ever committed was seen again.  Finally,
    in 1942 the state of Michigan announced they would send in an investigatory team to
    determine if there was any truth to these allegations.  A five person team consisting of
    doctors and psychologists arrived on July 16 of that year.  <br><br>Unfortunatly, on the first
    day of their investigations, there was an accident; a fire broke out in the upper floors of
    the building, which contained inmate dormitories, and spread quickly through the frail
    wooden structure.  Some of the ground floor survived, however, fire teams were unable to
    locate any survivors.  It is believed that all 73 inmates, 15 staff, Dr. Perry and the five person
    investigatory team were cremated that day in the fire.<br><br>
    After the incident, the case was quickly closed and the building left abandonded.  To this day,
    a burned out shell of the ground floor remains visible on the top of Perry Hill.  A rusty chain-link
    fence surrounds the overgrown property.<br><br>
    Mikal finishes cutting the fence and slips through. \"Let's go,\" Mikal summons you forward.
    The night surrounds the two of you as you slither
    like secret agents towards the black, forboding structure.  The air is heavy and foul, as if
    filled with an incense of evil.  It's difficult to remember how Mikal talked you into this;
    something about the glory and excitement of urban infiltration.  Mikal says he hasn't
    actually explored here before, but he heard from some others that it was interesting, although not too
    much to see.  Apparently the fire was pretty thorough.  The grass is high and uncut, filled with
    weeds and who knows what else.  Eventually you make it to the entry way; the moonlight
    shining down onto its burned out husk casts eerie shadows all around.  Mikal hesitates for a moment,
    and an owl cries out in the darkness.  \"Ok,\" he says quietly, \"Keep your flashlight low at all times.
    We don't want to attract attention.  Stick together, and watch for unsafe floors.  Don't climb any
    stairs.  The whole second floor and above is gone anyways, so there wouldn't be anything to see.
    Got it?\" You nod.";

    
    /********* TIMERS and STUFF **********/
    
	
	$MikalFuse = new MessageFuse();
	$MikalFuse->Messages[0] = "\"Hey!\" you hear Mikal yell out from above, \"Just stay put!  I'm going to get help!\""; // '
	$MikalFuse->Messages[1] = "The darkness seems to be animated by your invasion.  The entire building seems to moan, and a foul breeze rushes past you.";
	$MikalFuse->Messages[2] = "\"Who the hell are you?\" Mikal's cry shatters the silence.";
	$MikalFuse->Messages[3] = "\"Hey, what the...!\" Mikal's voice is cut off by a resounding thump.";
	$MikalFuse->Messages[4] = "";
	$MikalFuse->Messages[5] = "";
	$MikalFuse->Messages[6] = "";
	$MikalFuse->Messages[7] = "The silence is broken by a loud buzzing and a sudden screaming... It's Mikal!  It sounds like he's being tortured!";
	$MikalFuse->Messages[8] = "Mikal's bloodcurling screams echo throughout the complex, along with a loud buzzing sound.";
	$MikalFuse->Messages[9] = "Mikal's bloodcurling screams echo throughout the complex, along with a loud buzzing sound.";
	$MikalFuse->Messages[10] = "Mikal's bloodcurling screams echo throughout the complex, along with a loud buzzing sound.";
	$MikalFuse->Messages[11] = "Mikal's bloodcurling screams echo throughout the complex, along with a loud buzzing sound.";
	$MikalFuse->Messages[12] = "Mikal's bloodcurling screams echo throughout the complex, along with a loud buzzing sound.";
	$MikalFuse->Messages[13] = "Mikal's bloodcurling screams echo throughout the complex, along with a loud buzzing sound.";
	$MikalFuse->Messages[14] = "Mikal's bloodcurling screams echo throughout the complex, along with a loud buzzing sound.";
	$MikalFuse->Messages[15] = "Mikal's bloodcurling screams echo throughout the complex, along with a loud buzzing sound.";
	$MikalFuse->Messages[16] = "Mikal's screams have become hoarse, and begun to trail off.  He won't last much longer!";
	$MikalFuse->Messages[17] = "Mikal's screams have become hoarse, and begun to trail off.  He won't last much longer!";
	$MikalFuse->Messages[18] = "Mikal falls silent, and the buzzing ends shortly thereafter.  A heavy silence takes it place.";
	$MikalFuse->Loop = FALSE;
	$MikalFuse->oid = "MikalFuse";
	$_allobjs['MikalFuse'] =& $MikalFuse;

	$BleedFuse = new BleedFuse();
	$BleedFuse->oid = "BleedFuse";
	$_allobjs['BleedFuse'] =& $BleedFuse;

	$DarkFuse = new DarkFuse();
	$DarkFuse->oid = "DarkFuse";
	$_allobjs['DarkFuse'] =& $DarkFuse;
	$DarkFuse->fuseleft = 1;


/***** BEGIN GAME *****/
	$flashlight = new Switchable();
	$flashlight->oid = "flashlight";
	$_allobjs['flashlight'] =& $flashlight;
	$flashlight->switchlight = TRUE;

	$flashlight->name = "flashlight";
	$flashlight->desc = "A small six inch flashlight.";
	$flashlight->moveInto($_Me);

	$doorwayn = new Object();
	$doorwayn->oid ="doorwayn";
	$_allobjs['doorwayn'] =& $doorwayn;
	$doorwayn->name = "doorway";
	$doorwayn->desc = "The doorway to the north appears to have once been a nice set of double doors, probably
	leading to the dormitories.  The doors themselves are gone, and beyond the doorway there is merely
	piles of rubble -- burned wooden, shingles and boards.";
	$doorwayn->moveInto($_Startroom);
	
	$timberfloor = new Object();
	$timberfloor->oid = "timberfloor";
	$_allobjs['timberfloor'] =& $timberfloor;
	$timberfloor->name = "floor";
	$timberfloor->desc = "The ancient timbers are dark with soot that caked onto them when the building burned.
	Many of the timbers are partially burned or rotted, and they creak ominously when walked on.";
	$timberfloor->moveInto($_Startroom);


	// add some no-go rooms
	$srwest = new NoEntranceRoom();
	$srwest->oid = "srwest";
	$_allobjs['srwest'] =& $srwest;
	$srwest->name = "outside";
	$srwest->rejectMsg = "Mikal grabs your sleeve. \"Oh, come on,\" he says, \"It's not time to leave yet.\""; // '
	$srwest->moveInto($_Startroom);

	$srnorth = new NoEntranceRoom();
	$srnorth->oid = "srnorth";
	$_allobjs['srnorth'] =& $srnorth;
	$srnorth->name = "doorway";
	$srnorth->rejectMsg = "The debris completely blocks that path.";
	$srnorth->moveInto($_Startroom);


	$_Startroom->name = "Entryway";
	$_Startroom->desc = "You are standing in what was once the entryway into the building.  The ceiling is almost gone,
	destroyed by the fire.  Moonlight shines through, casting shadows on the walls, which are black
	with soot.  The " . $timberfloor->fixedObjRef() . " creaks and groans under your weight, the ancient timbers surprised at once
	again being tread on by human feet.  A " . $doorwayn->fixedObjRef() . " to the " . $_Startroom->addGo("srnorth", "north") . " has been completely burned up, what
	was once a hallway beyond it is now filled with impassable debris.  Hallways in seemingly better
	condition lead to the " . $_Startroom->addGo("main_north", "east") . " and " . $_Startroom->addGo("b2_mid", "south") . ".  To the " . $_Startroom->addGo("srwest", "west") . " lies the great outdoors, the deep grassy field which was
	once a manicured lawn.";
	$_Startroom->firstseen = "A chill runs down your spine as you survey the half-ruined building.  It stands as an unholy
	memory to a horror that almost seems alive.";


	$digitalcamera = new Object();
	$digitalcamera->oid = "digitalcamera";
	$_allobjs['digitalcamera'] =& $digitalcamera;
	$digitalcamera->name = "digital camera";
	$digitalcamera->desc = "Mikal uses his digital camera to record his adventures.  In fact, it was by showing pictures
	of previous adventures that he convinced you to go on this trip.";


	// *** create Mikal
	$Mikal = new MikalNPC();
	$Mikal->oid = "Mikal";
	$_allobjs['Mikal'] =& $Mikal;
	$Mikal->name = "Mikal";
	$Mikal->desc = "Your friend Mikal from biology class.  He invited you on this little adventure.  Currently
	decked out in black clothes, Mikal keeps his flashlight low and moves like a pro.";
	$Mikal->dontKnow = "\"Beats me,\" Mikal shrugs.";
	$Mikal->moveInto($_Startroom);
	// random messages
	$Mikal->randomMessages[0] = "Mikal examines some burnt floorboards.";
	$Mikal->randomMessages[1] = "Mikal raps lightly on the wall.";
	$Mikal->randomMessages[2] = "Mikal takes a picture with his " . $digitalcamera->fixedObjRef() . ".";
	$Mikal->randomMessages[3] ="\"This is a pretty spooky place,\" Mikal whispers.";
	$Mikal->randomMessages[4] = "Mikal paces slowly across the floor, listening to it creak and groan under his weight.";
	$Mikal->randomMessages[5] ="\"Whenever you're ready,\" Mikal says, \"Lead the way.\""; // '
	$Mikal->randomMessages[6] ="Mikal looks around contemplatively.";
	$Mikal->randomMessages[7] = "";
	$Mikal->randomMessages[8] = "";
	$Mikal->randomMessages[9] = "";
	$Mikal->itSelf = "himself";
	// ask about knowledge base
	$Mikal->askAddObject($digitalcamera, "\"I picked it up on Ebay cheap,\" Mikal says, \"I take it everywhere.  You never know when there's going
	to be a photo-perfect moment.\""); //'
	$Mikal->askAddObject($flashlight, "Mikal nods. \"I keep a bunch of these around for infiltrations.  Fresh batteries and all.  Very reliable, no need for concern.\"");
	$Mikal->askAddObject($doorwayn, "\"Wow.  Imagine being one of the poor dudes trapped in the fire,\" he says in awe.");
	$Mikal->askAddObject($timberfloor, "\"This floor makes me nervous.  We're definetly not going up.  Falling through onto the ground
	is one thing, but falling through down a story would be bad news.\""); // '
	$Mikal->askAddObject($Mikal, "\"This is not a great time to be asking about my life story,\" he says.");
	$Mikal->askAddTopic("the Perry House", "\"I read about this place online,\" he says, \"Apparently they used to conduct
	crazy experiments here or something.  You know, the whole mad scientist bit.  Well, some
	dudes came in to check it out and the whole place burned down.  I bet, \" he leans closer,
	\"That the doctor started it.  He knew he was going down, so he took everybody else with him.\"");


	$digitalcamera->moveInto($Mikal);


	// *** main_north

	$doorwaye = new Object;
	$doorwaye->oid = "doorwaye";
	$_allobjs['doorwaye'] =& $doorwaye;
	$doorwaye->name = "closed door";
	$doorwaye->desc = "The doorway to the east appears to be jammed shut.  You are unable to open it.";
	$Mikal->askAddObject($doorwaye, "\"Probably a bunch of debris from the fire leaning up against it.  I doubt we'll get it open,\" he says in awe."); // '

	$spiderwebs = new Object;
	$spiderwebs->oid = "spiderwebs";
	$_allobjs['spiderwebs'] =& $spiderwebs;
	$spiderwebs->name = "spiderwebs";
	$spiderwebs->desc = "The spiderwebs seem ancient, filled with dust.  You don't see any spiders; in fact, this whole place seems pretty dead.";


	$main_north = new Room();
	$main_north->oid = "main_north";
	$_allobjs['main_north'] =& $main_north;
	$main_north->name = "Hallway";
	$main_north->desc = "You are standing in an L shaped hallway leaving to the " . $main_north->addGo("_Startroom", "west") .
	" and the " . $main_north->addGo("main_mid", "south") . ".  A " . $doorwaye->fixedObjRef() ." blocks passage to the east.  The ceiling is more intact here, but moonlight still shines through.
	Scorch marks line the walls.  Several large " . $spiderwebs->fixedObjRef() . " grace the edges of the floor.";

	$doorwaye->moveInto($main_north);
	$spiderwebs->moveInto($main_north);

	// *** main_mid
	
	$paintings = new Object();
	$paintings->oid = "paintings";
	$_allobjs['paintings'] =& $paintings;
	$paintings->name = "paintings";
	$paintings->desc = "A couple of the paintings near the north end of the hall are damaged beyond recognition.
	Near the south end, you can identify Starry Night by Van Gogh and another painting which shows
	an artistically done sunflower.  There is also a portrait of a man dressed in fine clothes.  The portrait is
	very creepy -- it may just be the fire damage, but the man in the portrait seems to have a very
	evil smirk. All the paintings have been damaged by the fire.";
	$Mikal->askAddObject($paintings, "\"Nice art for a creepy place like this,\" he muses.");



	$main_mid = new Room();
	$main_mid->oid = "main_mid";
	$_allobjs['main_mid'] =& $main_mid;
	$main_mid->name = "Hallway";
	$main_mid->desc = "You are standing in a hallway which proceeds " . $main_mid->addGo("main_north", "north") . " and " .
	$main_mid->addGo("main_south", "south") . ".  The walls are adorned with what
	were once probably nice " . $paintings->fixedObjRef() . ".  The heat from the fire seems to have disfigured them, distorting the
	image and peeling the canvas.";

	$paintings->moveInto($main_mid);

	// *** main_south
	
	$ms_door = new Object();
	$ms_door->oid = "ms_door";
	$_allobjs['ms_door'] =& $ms_door;
	$ms_door->name = "smooth door";
	$ms_door->desc = "The wooden door has suffered minor burns from the fire but appears strong and intact.  There
		is no knob, lock or any visible method of opening the door.";
	$Mikal->askAddObject($ms_door, "\"Maybe if we had a crowbar we could get this open,\" he says, \"But there's probably another way around.\""); // '

	$ms_closet = new Lockable();
	$ms_closet->oid = "ms_closet";
	$_allobjs['ms_closet'] =& $ms_closet;
	$ms_closet->name = "small closet";
	$ms_closet->desc = "The closet is about four feet tall and two feet wide, made of attractive wooden panelling.  There is a small keyhole embedded into the closet.  ";
	$ms_closet->isFixed = TRUE;
	$ms_closet->isLocked = TRUE;
	$ms_closet->keylessUnlock = FALSE;
	$Mikal->askAddObject($ms_closet, "\"These little closets tend to hold the most interesting stuff,\" he says, \"I have a policy of never taking anything out though -- otherwise I'd be no better than a looter or vandal.\""); // '

	$main_south = new Room();
	$main_south->oid = "main_south";
	$_allobjs['main_south'] =& $main_south;

	$main_south->name = "End of Hallway";
	$main_south->desc = "You are standing at the end of the hallway which leaves to the " . $main_south->addGo("main_mid", "north") . ".  To the west, a " .
	$ms_door->fixedObjRef() . " with no handle is closed.  There is a " . $ms_closet->fixedObjRef() . " in the south wall.";


	$ms_door->moveInto($main_south);
	$ms_closet->moveInto($main_south);



	/******** BASEMENT 2 ************/


	$b2_floor = new BloodFloor();
	$b2_floor->oid = "b2_floor";
	$_allobjs['b2_floor'] =& $b2_floor;
	$b2_floor->name = "floor";
	$b2_floor->desc = "The cold concrete floor is covered in ancient blood stains which betray a horror long past.  New blood from your leg wound mixes with the old blood on the floor, garnished with debris from your fall.";
	$b2_floor->isSit = FALSE;
	$b2_floor->canContain = FALSE;
	$b2_floor->outMsg = "You painfully pull yourself up.  Your bleeding leg buckles, but clawing against the cold wall, you manage to stand.";

	$b2_mid = new Fallroom();
	$b2_mid->oid = "b2_mid";
	$_allobjs['b2_mid'] =& $b2_mid;
	$b2_mid->isblood = TRUE;
	$b2_mid->name = "Basement 2, Landing zone";
	$b2_mid->desc = "You are in an old north-" . $b2_mid->addGo("b2_south", "south") . " hallway.  This section seems to be in even worse repair than the
	section up top, if that's possible.  The " . $b2_floor->fixedObjRef() . ", covered in blood and debris, is made of solid concrete.
	Above you is a hole in the ceiling, indicating a floor above this one.  Above that is the original hole
	you fell through, probably thirty feet up.";

	$b2_floor->moveInto($b2_mid);

	$b2swdoor = new Door();
	$b2swdoor->oid = "b2swdoor";
	$_allobjs['b2swdoor'] =& $b2swdoor;
	$b2swdoor->name = "door";
	$b2swdoor->desc = "An old, wooden door with a small rusty knob. ";


	$b2_south = new BloodRoom();
	$b2_south->oid = "b2_south";
	$_allobjs['b2_south'] =& $b2_south;
	$b2_south->name = "Basement 2, South Hall";
	$b2_south->desc = "You are at the south end of a hallway departing to the " . $b2_south->addGo("b2_mid", "north") . ". There is a " . $b2swdoor->fixedObjRef() . '<<global $_allobjs; return " " . ($_allobjs[\'b2swdoor\']->isopen ? "(open) " : "(closed) ");>>' . " to the " . $b2_south->addGo("b2swdoor", "west") . ". An open archway leads " . $b2_south->addGo("b2e", "east") . ".";




	$b2swroom = new BloodRoom();
	$b2swroom->oid = "b2swroom";
	$_allobjs['b2swroom'] =& $b2swroom;

	$b2swdoor->connectRooms($b2_south, $b2swroom);

	// supply room

	$b2swtrinkets = new Object();
	$b2swtrinkets->oid = "b2swtrinkets";
	$_allobjs['b2swtrinkets'] =& $b2swtrinkets;
	$b2swtrinkets->name = "trinkets";
	$b2swtrinkets->aname = "some trinkets";
	$b2swtrinkets->desc = "You have no idea what these things are, or what they are for.";
	$b2swtrinkets->isListed = TRUE; // for shelf listing

	$b2swshelves = new Surface();
	$b2swshelves->oid = "b2swshelves";
	$_allobjs['b2swshelves'] =& $b2swshelves;
	$b2swshelves->name = "shelves";
	$b2swshelves->desc = "The dusty shelves are covered with all manner of strange " . $b2swtrinkets->fixedObjRef() . " -- various parts, vials
		and other unidentifiable objects.  ";
	$b2swshelves->isFixed = TRUE;


	$b2swroom->name = "Supply Room";
	$b2swroom->desc = "You are standing in a small, dusty room.  A " . $b2swdoor->fixedObjRef() . " leads " . $b2swroom->addGo("b2swdoor", "out") . " back to the main
	hallway.  Other than that, you are surrounded by " . $b2swshelves->fixedObjRef() . " built into the walls.  ";
	//A variety
	//of bizarre " . $b2swtrinkets->fixedObjRef() . " sit on the " . $b2swshelves->fixedObjRef() . ".";

	$b2swshelves->moveInto($b2swroom);
	$b2swtrinkets->moveInto($b2swshelves);


	$bandage = new Wearable();
	$bandage->oid = "bandage";
	$_allobjs['bandage'] =& $bandage;
	$bandage->name = "cloth bandage";
	$bandage->isblood = FALSE;
	$bandage->desc = "A roll of cloth bandage, enough to dress one wound. " . '<<if ($_allobjs[\'bandage\']->isblood) return "The bandage is bloody. "; else return "The bandage is clean. ";>>';
	$bandage->wearMsg = "You tie the bandage around your wounded leg and tighten it up well.  That should stop the bleeding!";
	$bandage->moveInto($b2swshelves);

	// b2 east
	$b2e = new BloodRoom();
	$b2e->oid = "b2e";
	$_allobjs['b2e'] =& $b2e;
	
	$b2e_fridge = new Openable();
	$b2e_fridge->oid = "b2e_fridge";
	$_allobjs['b2e_fridge'] =& $b2e_fridge;
	$b2e_fridge->isFixed = TRUE;
	$b2e_fridge->isOpen = FALSE;
	$b2e_fridge->name = "refrigerator";
	$b2e_fridge->desc = "An old GE refridgerator, probably from the 30s or 40s.";
	$b2e_fridge->moveInto($b2e);

	$b2e_fridge_nasty = new Object();
	$b2e_fridge_nasty->oid = "b2e_fridge_nasty";
	$_allobjs['b2e_fridge_nasty'] =& $b2e_fridge_nasty;
	$b2e_fridge_nasty->name = "nasty food";
	$b2e_fridge_nasty->aname = "some nasty food";
	$b2e_fridge_nasty->desc = "A variety of things that may have once been food have been reduced to discolored blobs in the refrigerator.  A horrid stench wafts out and invades the room.";
	$b2e_fridge_nasty->isListed = TRUE;
	$b2e_fridge_nasty->moveInto($b2e_fridge);


	$b2e_counter = new Surface();
	$b2e_counter->oid = "b2e_counter";
	$_allobjs['b2e_counter'] =& $b2e_counter;
	$b2e_counter->name = "countertop";
	$b2e_counter->isFixed = TRUE;
	$b2e_counter->desc = "A faded, wooden countertop covered in ancient spill stains. ";
	$b2e_counter->moveInto($b2e);

	$b2e_blood = new VisionObject();
	$b2e_blood->oid = "b2e_blood";
	$_allobjs['b2e_blood'] =& $b2e_blood;
	$b2e_blood->name = "coffee pot";
	$b2e_blood->desc = "An old coffee pot, filled with what appears to be dark, thick blood. ";
	$b2e_blood->triggermsg = '<<$_allobjs[\'b2e_blood\']->moveOut(); $_allobjs[\'b2e_blood2\']->moveInto($_allobjs[\'b2e\']); >>' . "Your hand touches the cold pot, and the room is transformed with a flash of light.  You are in a bright room with an operating table.  A patient a latched down with restraints, and she struggles against them.  A doctor in a lab coat walks up and sits on a stool next to her.  He sets an empty coffee pot on the floor, and pulls a scapel.  He then slices her wrist as her screams echo through the bleached room.  Her blood drips quickly into the coffee pot.  Her screams become hoarse and weak as the coffee pot begins to fill with her red blood.  The doctor nods and makes notes on his notepad.  Finally, she is done; her body slumps on the table as the bleeding begins to ebb.  The doctor smiles.  \"Very good,\" he mutters to no one in particular.  He picks up the nearly full coffee pot and heads for the door.  A flash returns you to the darkness of the break room, and your eyes take a moment to readjust.";
	$b2e_blood->moveInto($b2e_counter);
	
	$b2e_blood2 = new Object();
	$b2e_blood2->oid = "b2e_blood2";
	$_allobjs['b2e_blood2'] =& $b2e_blood2;
	$b2e_blood2->name = "broken coffeepot";
	$b2e_blood2->desc = "Pieces of a shattered coffeepot are scattered around the floor.  Thick, red blood has splattered out of it and covers the cold floor.";
	$b2e_blood2->isListed = TRUE;
	// not in any place to start with

	$b2e_clock = new Object();
	$b2e_clock->oid = "b2e_clock";
	$_allobjs['b2e_clock'] =& $b2e_clock;
	$b2e_clock->name = "clock";
	$b2e_clock->desc = "An old wall clock whose hands are permanently fixed at 3:17.";
	$b2e_clock->moveInto($b2e);

	$b2e_table = new Surface();
	$b2e_table->oid = "b2e_table";
	$_allobjs['b2e_table'] =& $b2e_table;
	$b2e_table->name = "table";
	$b2e_table->desc = "The table is a simple, round wooden platform about a meter across, standing on four metal legs about half a meter tall.";
	$b2e_table->isFixed = TRUE;
	$b2e_table->moveInto($b2e);

	$b2e_chair = new SitLayItem();
	$b2e_chair->oid = "b2e_chair";
	$_allobjs['b2e_chair'] =& $b2e_chair;
	$b2e_chair->name = "chair";
	$b2e_chair->desc = "A simple, wooden chair. ";
	$b2e_chair->isSit = TRUE;
	$b2e_chair->moveInto($b2e);

	$b2e->name = "Break Room";
	$b2e->desc = "You are standing in a cold, ancient break room that seems frozen in time.  An old, yellow " . $b2e_fridge->fixedObjRef() . " sits next to a faded " . $b2e_counter->fixedObjRef() . ".  In the center of the room, a small round " . $b2e_table->fixedObjRef() . " is graced by a simple " . $b2e_chair->fixedObjRef() . ".  A " . $b2e_clock->fixedObjRef() . " hangs on the wall.  An archway leads " . $b2e->addGo("b2_south", "west") . ".";


}

?>
