RESTAdventure, an improved version of PHPAdventure! by Olivier Berger - Telecom SudParis
=============

This is a POC for a REST adventure game, reusing code from PHPAdventure! version 1.1.


Copyright (c) 2003 Steven Kollmansberger
Copyright 2016 Olivier Berger - Telecom SudParis

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

See the COPYING file for full license text.


1. Requirements and installation
--------------------------------

Server:  An HTTP daemon and a recent version of PHP (greater than 4.1.0 probably) with 
session support is required.
Client:  Cookies are required.  (Unless your PHP is configured to use non-cookie sessions)
Game Programmer:  A working knowledge of PHP is necessary for all but the simplest games.

1) You should make a directory that is processed by php and put all of the ad_*.php files 
and encrypt.php in that.

These files need to be readable by your php engine but the outside world never needs to access them.  
They will be included through your game file.  

2) To test your installation, place game_std.php in the directory and open
it over the http daemon.  This should activate the engine and display the page.

You should see something like this (in nice, pretty stylized html, of course):

Default Room							A PHPAdventure! Game (1.1)

You are in a blank, empty, nondescript room. 	About
There is nothing to do. Better add some stuff!		Quit
									Restart

									Inventory

									You are carrying nothing.
									

If you wish to host a pre-made game, simply copy the game file(s) into the directory
and have users open the main game file.







2. Programming with PHPAdventure!

The file game_std.php is a starting point for creating a game.
Included is a sample mini-game called "The Golden Skull" (game_goldenskull.php)
You can follow the tutorial and see how this game was created, one step at a time.

In addition, each class in the system is commented at the beginning of the class
definition.  For information on the Game class (from which _Game is instantiated),
the Object class (from which all other objects are derived) and the Fuse class
(which allows custom timers to be created), see ad_base.php

For all other classes, see ad_classes.php

You should start by simply making some rooms and ordinary types of objects
in those rooms.  Each class documentation shows properties you can set
to change the behavior of the objects.

In order to create new functionality, you will need to derive classes.
Refer to the Class Framework file for information on making a derived class.
You can also look in the source at examples.





3. Contact Information

Note that I am not an expert PHP programmer, so if you find the quality of code wanting,
don't whine about it, fix it! :)

E-Mail: steven777400@users.sourceforge.net






restapp
=======


SOLUTION :
 - GET /extra/
 
TODO :
- Replace the descriptions by placeholders to be processed by functions as twig extensions ?

  array('You're standing in the bright sunlight just outside of <<cave>>. Nearby, a <<arch>> stands between you and the rest of the universe.',

     'cave' => array ( 'desc' => 'a large, dark, forboding cave', 'item' => 'cave'),
     'arch' => array ( 'desc' => 'glowing archway', 'item' => 'arch')

- Inventory as cookies
